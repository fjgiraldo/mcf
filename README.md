# Madison Capital Funding

## Setup

After the theme has been cloned and before the theme gets activated into the site you need to make sure all the dependencies are activated which means: 

- Plugins via composer
- Theme Dependencies
    - Node
    - Composer
    - Bower

## Plugins installation

On the root of the project there is a `composer.json` file that contains the information about the required plugins in order to install them you need to run:


```bash
composer install
```

If you face any issue during this make sure to have the correct permisions to clone some premium plugins that are stored on bitbucket.

## Theme dependencies

Everything is located inside of the theme so you need to install the dependencies directly from there. 
Follow the theme Readme file instruction to continue with the installation.

Theme is located at: 

```bash
wp-content/themes/madison 
```
