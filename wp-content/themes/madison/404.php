<?php get_header(); ?>

<main class="main-container default-page-container" role="main" itemprop="mainContentOfPage">

	<div class="not-found-page">
		<div class="container">
			<h2 class="not-found-title">Error 404: Page Not Found.</h2>
		</div>
	</div>

</main>

<?php get_footer(); ?>
