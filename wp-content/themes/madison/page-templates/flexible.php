<?php
/**
 * Template Name: Flexible
 */
use Lean\Load;

get_header();
?>

<main role="main" itemprop="mainContentOfPage" class="modules-container">

	<?php
	while ( have_posts() ) :
		the_post();

		$hero_is_visible = get_field( 'hero_is_visible' );

		Load::organism( 'hero/hero', [
			'is_visible' => $hero_is_visible,
			'title' => get_field( 'hero_title' ),
			'content' => get_field( 'hero_content' ),
			'bg_image' => get_field( 'hero_background_image' ),
			'box_color' => get_field( 'hero_box_color' ),
		]);
		?>

		<?php get_template_part( 'page-templates/flexible', 'part' ); ?>

	<?php endwhile; ?>
	
</main>

<?php
get_footer();
