<?php
/**
 * Template Name: Transactions
 */
use Lean\Load;

get_header();
?>

<main class="main-container" role="main" itemprop="mainContentOfPage">
	<?php
	while ( have_posts() ) {
		the_post();

		Load::organism( 'transactions/transactions', [
			'title' => get_field( 'transactions_title' ),
			'introduction' => get_field( 'transactions_introduction' ),
		]);

		get_template_part( 'page-templates/flexible', 'part' );
	}
?>
<main>

<?php
get_footer();
