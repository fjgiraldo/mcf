<?php
use Lean\Load;

while ( have_rows( 'flexible_modules' ) ) {
	the_row();
	switch ( get_row_layout() ) {
		case 'attributes':
			Load::organism( 'attributes/attributes', [
				'title' => get_sub_field( 'attributes_title' ),
				'introduction' => get_sub_field( 'attributes_introduction' ),
				'boxes' => get_sub_field( 'attributes_boxes' ),
				'button' => get_sub_field( 'attributes_button' ),
				'bg_color' => get_sub_field( 'attributes_bg_color' ),
				'icon_spacing' => get_sub_field( 'attributes_icon_spacing' ),
			]);
		break;
		case 'awards':
			Load::organism( 'awards/awards', [
				'title' => get_sub_field( 'awards_title' ),
				'boxes' => get_sub_field( 'awards_boxes' ),
				'button' => get_sub_field( 'awards_button' ),
				'bg_color' => get_sub_field( 'awards_bg_color' ),
			]);
		break;
		case 'capabilities_teaser':
			Load::organism( 'capabilities-teaser/capabilities-teaser', [
				'capabilities' => get_sub_field( 'capabilities_teasers' ),
				'bg_color' => get_sub_field( 'capabilities_bg_color' ),
			]);
		break;
		case 'copy_block':
			Load::organism( 'copy-block/copy-block-media', [
				'title' => get_sub_field( 'copy_block_title' ),
				'content' => get_sub_field( 'copy_block_content' ),
				'link' => get_sub_field( 'copy_block_link' ),
				'media_type' => get_sub_field( 'copy_block_media_type' ),
				'media_image' => get_sub_field( 'copy_block_image' ),
				'media_video' => get_sub_field( 'copy_block_video' ),
				'media_video_brightcove' => get_sub_field( 'copy_block_brightcove_video' ),
				'media_video_image' => get_sub_field( 'copy_block_video_image' ),
				'bg_color' => get_sub_field( 'copy_block_bg_color' ),
			]);
		break;
		case 'copy_block_text':
			Load::organism( 'copy-block/copy-block-text', [
				'title' => get_sub_field( 'copy_block_text_title' ),
				'content' => get_sub_field( 'copy_block_text_content' ),
				'link' => get_sub_field( 'copy_block_text_link' ),
				'bg_color' => get_sub_field( 'copy_block_text_bg_color' ),
			]);
		break;
		case 'deal_announcements':
			Load::organism( 'deal-announcements/deal-announcements', [
				'title' => get_sub_field( 'deal_announcements_title' ),
				'introduction' => get_sub_field( 'deal_announcements_introduction' ),
				'vertical' => get_sub_field( 'deal_announcements_vertical' ),
				'bg_color' => get_sub_field( 'deal_announcements_bg_color' ),
			]);
		break;
		case 'description':
			Load::organism( 'description/description', [
				'title' => get_sub_field( 'description_title' ),
				'image' => get_sub_field( 'description_image' ),
				'content' => get_sub_field( 'description_content' ),
				'button' => get_sub_field( 'description_button' ),
				'bg_color' => get_sub_field( 'description_bg_color' ),
				'box_color' => get_sub_field( 'description_box_color' ),
			]);
		break;
		case 'facts_&_figures':
			Load::organism( 'facts-figures/facts-figures', [
				'title' => get_sub_field( 'facts_&_figures_title' ),
				'boxes' => get_sub_field( 'facts_&_figures_boxes' ),
				'bg_color' => get_sub_field( 'facts_&_figures_bg_color' ),
			]);
		break;
		case 'hero_lead_in':
			Load::organism( 'hero-lead-in/hero-lead-in', [
				'title' => get_sub_field( 'hero_lead_in_title' ),
				'image' => get_sub_field( 'hero_lead_in_image' ),
				'content' => get_sub_field( 'hero_lead_in_content' ),
				'button' => get_sub_field( 'hero_lead_in_button' ),
				'bg_color' => get_sub_field( 'hero_lead_in_bg_color' ),
				'box_color' => get_sub_field( 'hero_lead_in_box_color' ),
				'box_position' => get_sub_field( 'hero_lead_in_box_position' ),
				'separation_bar' => get_sub_field( 'hero_lead_in_separation_bar' ),
			]);
		break;
		case 'history':
			Load::organism( 'history-timeline/history-timeline', [
				'title' => get_sub_field( 'history_title' ),
				'introduction' => get_sub_field( 'history_introduction' ),
				'entry_points' => get_sub_field( 'history_entry_points' ),
				'bg_color' => get_sub_field( 'history_bg_color' ),
			]);
		break;
		case 'image_carousel':
			Load::organism( 'image-carousel/image-carousel', [
				'title' => get_sub_field( 'image_carousel_title' ),
				'description' => get_sub_field( 'image_carousel_description' ),
				'slides' => get_sub_field( 'image_carousel_slider' ),
				'bg_color' => get_sub_field( 'image_carousel_bg_color' ),
			]);
		break;
		case 'insights':
			Load::organism( 'insights/insights', [
				'title' => get_sub_field( 'insights_title' ),
				'introduction' => get_sub_field( 'insights_introduction' ),
				'bg_color' => get_sub_field( 'insights_bg_color' ),
			]);
		break;
		case 'job_opportunities':
			Load::organism( 'job-opportunities/job-opportunities', [
				'title' => get_sub_field( 'job_opportunities_title' ),
				'introduction' => get_sub_field( 'job_opportunities_introduction' ),
				'bg_color' => get_sub_field( 'job_opportunities_bg_color' ),
			]);
		break;
		case 'media':
			Load::organism( 'media/media', [
				'title' => get_sub_field( 'media_title' ),
				'introduction' => get_sub_field( 'media_introduction' ),
				'bg_color' => get_sub_field( 'media_bg_color' ),
			]);
		break;
		case 'media_&_insights_highlight':
			Load::organism( 'media-insights-highlight/media-insights-highlight', [
				'boxes' => get_sub_field( 'media_&_insights_highlight_posts' ),
				'button' => get_sub_field( 'media_&_insights_highlight_button' ),
				'bg_color' => get_sub_field( 'media_&_insights_highlight_bg_color' ),
			]);
		break;
		case 'menu':
			Load::organism( 'menu/menu', [
				'title' => get_sub_field( 'menu_title' ),
				'text' => get_sub_field( 'menu_text' ),
				'image' => get_sub_field( 'menu_image' ),
				'links' => get_sub_field( 'menu_links' ),
				'box_color' => get_sub_field( 'menu_box_color' ),
				'box_alignment' => get_sub_field( 'menu_box_alignment' ),
				'bg_color' => get_sub_field( 'menu_bg_color' ),
			]);
		break;
		case 'process':
			Load::organism( 'process/process', [
				'title' => get_sub_field( 'process_title' ),
				'steps' => get_sub_field( 'process_steps' ),
				'bg_color' => get_sub_field( 'process_bg_color' ),
			]);
		break;
		case 'products':
			Load::organism( 'products/products', [
				'title' => get_sub_field( 'products_title' ),
				'introduction' => get_sub_field( 'products_introduction' ),
				'products' => get_sub_field( 'products_products' ),
				'bg_color' => get_sub_field( 'products_bg_color' ),
			]);
		break;
		case 'quote':
			Load::organism( 'quote/quote', [
				'quotes' => get_sub_field( 'quote_quotes' ),
			]);
		break;
		case 'sectors':
			Load::organism( 'sectors/sectors', [
				'title' => get_sub_field( 'sectors_title' ),
				'subsectors' => get_sub_field( 'sectors_subsectors' ),
				'button' => get_sub_field( 'sectors_button' ),
				'button_file_label' => get_sub_field( 'sectors_button_file_label' ),
				'button_file' => get_sub_field( 'sectors_button_file' ),
				'bg_color' => get_sub_field( 'sectors_bg_color' ),
			]);
		break;
		case 'slideshow':
			Load::organism( 'slideshow/slideshow', [
				'title' => get_sub_field( 'slideshow_title' ),
				'introduction' => get_sub_field( 'slideshow_introduction' ),
				'initial_box_title' => get_sub_field( 'slideshow_initial_box_title' ),
				'initial_box_description' => get_sub_field( 'slideshow_initial_box_description' ),
				'slides' => get_sub_field( 'slideshow_slides' ),
				'bg_color' => get_sub_field( 'slideshow_bg_color' ),
			]);
		break;
		case 'team':
			Load::organism( 'team/team', [
				'title' => get_sub_field( 'team_title' ),
				'introduction' => get_sub_field( 'team_introduction' ),
				'category_chosen' => get_sub_field( 'team_category_chosen' ),
				'members_by_department' => get_sub_field( 'team_members_by_category' ),
				'members_by_vertical' => get_sub_field( 'team_members_by_vertical' ),
				'bg_color' => get_sub_field( 'team_bg_color' ),
			]);
		break;
		case 'transactions_highlights':
			Load::organism( 'transactions-highlights/transactions-highlights', [
				'title' => get_sub_field( 'transactions_highlights_title' ),
				'boxes' => get_sub_field( 'transactions_highlights_posts' ),
				'button' => get_sub_field( 'transactions_highlights_button' ),
			]);
		break;
		case 'verticals':
			Load::organism( 'verticals/verticals', [
				'title' => get_sub_field( 'verticals_title' ),
				'boxes' => get_sub_field( 'verticals_boxes' ),
				'bg_color' => get_sub_field( 'verticals_bg_color' ),
			]);
		break;
		case 'verticals_teaser':
			Load::organism( 'verticals-teaser/verticals-teaser', [
				'title' => get_sub_field( 'verticals_teaser_title' ),
				'verticals' => get_sub_field( 'verticals_teaser_verticals' ),
				'bg_color' => get_sub_field( 'verticals_teaser_bg_color' ),
			]);
		break;
	}
}
