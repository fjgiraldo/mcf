<?php
use Lean\Load;
use Madison\Modules\Posts\Posts as Post;

$args = wp_parse_args( $args, [
  'boxes' => [],
  'button' => [],
  'bg_color' => 'blue',
]);

if ( empty( $args['boxes'] ) ) {
	return;
}

Load::molecule( 'navigation/link-anchor', [
	'title' => '',
	'default' => 'media-insights-highlight',
]);
?>

<section class="o__media-insights-highlight o__media-insights-highlight--<?php echo esc_attr( $args['bg_color'] ); ?>">

	<?php
	Load::atom( 'separator/separator', [
		'type' => 'section',
	]);
	?>

  <div class="container container--no-padding">

		<div class="o__media-insights-highlight-boxes">
			<?php
			foreach ( $args['boxes'] as $post_id ) {
				Load::molecule( 'box/media-insight-box', Post::get_data( $post_id ) );
			}
			?>
		</div>

		<?php if ( ! empty( $args['button']['url'] ) ) : ?>
			<div class="o__media-insights-highlight-button">
			<?php
			Load::atom( 'buttons/link-as-button', [
				'link' => $args['button'],
				'type' => 'blue' === $args['bg_color'] ? 'primary' : 'secondary',
			]);
			?>
			</div>
		<?php endif; ?>

	</div>
</section>
