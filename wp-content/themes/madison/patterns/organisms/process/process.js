const from = require( 'array.from' );

/**
 * Starts the slider and the timeline animations.
 */
function processSlider() {
  const sectionContainer = document.querySelector( '.o__process' );
  if ( !sectionContainer ) {
    return;
  }

  const sliderContainer = sectionContainer.querySelector( '.o__process-steps' );
  if ( !sliderContainer ) {
    return;
  }

  const slides = from( sliderContainer.querySelectorAll( '.m__process-step' ) );
  const navIcon = sectionContainer.querySelector( '.o__process-slider-nav-icon' );
  const steps = from( sectionContainer.querySelectorAll( '.o__process-menu-step' ) );
  const activeClass = 'active';  
  const $slider = $( sliderContainer );

  initSlider();
  setActiveStepClass();

  /**
   * Initiates the slider.
   */
  function initSlider() {
    if ( slides.length > 0 ) {
      $slider.owlCarousel({
        items: 1,
        nav: true,
        dots: false,
        center: true,
        smartSpeed: 550,
        margin: 20,
        stagePadding: 5,
        navText: [navIcon.innerHTML, navIcon.innerHTML],
        URLhashListener:true,
        startPosition: `#${steps[0]}`,
        onChanged: onSlideChanged,          
      });    
    }
  }

  /**
   * Sets the active class to the clicked step.
   */
  function setActiveStepClass() {
    if ( steps.length <= 0 ) {
      return;
    }
    steps.forEach( ( step ) => {
      step.addEventListener( 'click', () => { 
        activateStep( step );
      });
    });
  }

  /**
   * Sets the active class to the step when the next and previous slider buttons are clicked.
   *
   * @param {object} event - the changed data.
   */
  function onSlideChanged( event ) {
    let index = parseInt( event.item.index ); 
    index = isNaN( index ) ? 0 : index;
    const currentStep = steps[index];
    if ( !currentStep ) {
      return;
    }
    activateStep( currentStep );
  }  

  /**
   * Remove the active class to all years and sets the active class to the chosen year.
   *
   * @param {element} actualStep - the actual Step chosen.
   * @param {number} index - the actual Step position in the Steps array.
   */
  function activateStep( actualStep ) {
    steps.forEach( ( step ) => step.classList.remove( activeClass ) );
    actualStep.classList.add( activeClass );
  }
}

module.exports = processSlider;