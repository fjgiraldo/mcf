<?php
use Lean\Load;

$args = wp_parse_args( $args, [
	'title' => '',
	'steps' => [],
	'bg_color' => 'blue',
]);

if ( empty( $args['steps'] ) ) {
	return;
}

Load::molecule( 'navigation/link-anchor', [
	'title' => $args['title'],
	'default' => 'process',
]);
?>

<section class="o__process o__process--<?php echo esc_attr( $args['bg_color'] ); ?>">
	<div class="container container--no-padding container--padding-mobile">

		<?php
		$theme = 'blue' === $args['bg_color'] ? 'light' : 'dark';

		Load::molecule( 'content/content', [
			'title' => $args['title'],
			'theme' => $theme,
			'aligned' => 'centered',
		]);
		?>

		<div class="o__process-menu-steps">  

			<?php foreach ( $args['steps'] as $index => $step ) : ?>
				<div class="o__process-menu-step <?php if ( 0 === $index ) { echo 'active'; } ?>">
					<a class="o__process-menu-step-title" href="#step<?php echo esc_attr( $index ); ?>"><?php echo esc_html( $step['title'] ); ?></a>
					<?php use_icon( 'side-arrow-gray', 'o__process-menu-step-arrow' ); ?>
				</div>
			<?php endforeach; ?>

		</div>		

		<div class="o__process-steps owl-carousel owl-theme">  

			<?php
			foreach ( $args['steps'] as $index => $step ) {
				Load::molecule( 'process-step/process-step', [
					'index' => $index,
					'icon' => $step['icon'],
					'title' => $step['title'],
					'description' => $step['description'],
					'bg_color' => $args['bg_color'],
				]);
			}
			?>

		</div>

		<span class="o__process-slider-nav-icon"><?php use_icon( 'side-arrow-lightblue' ); ?></span>

	</div>
</section>
