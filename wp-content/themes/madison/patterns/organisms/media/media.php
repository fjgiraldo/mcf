<?php
use Lean\Load;
use Madison\Modules\Posts\Posts;

$args = wp_parse_args( $args, [
	'title' => '',
	'introduction' => '',
	'bg_color' => 'blue',
]);

Load::molecule( 'navigation/link-anchor', [
	'title' => $args['title'],
	'default' => 'media',
]);
?>

<section class="o__media o__media--<?php echo esc_attr( $args['bg_color'] ); ?>">

	<div class="container container--no-padding container--padding-mobile">

		<?php
		$theme = 'blue' === $args['bg_color'] ? 'light' : 'dark';

		Load::molecule( 'content/content', [
			'title' => $args['title'],
			'content' => $args['introduction'],
			'theme' => $theme,
			'aligned' => 'centered',
		]);
		?> 

		<div class="o__media-filters">

			<span class="o__media-filters-icon"><?php use_icon( 'side-arrow-lightblue' ); ?></span>

			<select class="o__media-filters-year" id="transactions_by_year">
				<option value="none">Filter by year</option>
				<option value="all">All</option>
				<?php
				$years = Posts::get_all_years();
				foreach ( $years as $year ) :
				?>
				<option value="<?php echo esc_attr( $year ); ?>"><?php echo esc_html( $year ); ?></option>
				<?php endforeach; ?>
			</select>
			
			<select class="o__media-filters-media-type" id="transactions_by_media_type">
				<option value="none">Filter by media type</option>
				<option value="all">All</option>		
				<?php
				$categories = Posts::get_all_category_terms();
				foreach ( $categories as $category ) :
				?>
				<option value="<?php echo esc_attr( $category->slug ); ?>"><?php echo esc_html( $category->name ); ?></option>
				<?php endforeach; ?>
			</select>
			
			<?php
			Load::molecule( 'search/search-form', [
				'class' => 'o__media-filters-search-input',
			]);
			?>

		</div>

		<div class="o__media-posts">
		</div>

		<div class="loader">
			<div class="loader-animation"></div>
		</div>		

		<div class="container o__media-button-container">
		<?php
		Load::atom( 'buttons/button', [
			'label' => 'More',
			'type' => 'secondary',
			'class' => 'o__media-view-more-button',
		]);
		?>
		</div>	

	</div>
</section>
