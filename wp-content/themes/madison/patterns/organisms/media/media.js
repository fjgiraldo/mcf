const $ = require( 'jquery' );

/**
 * Initialize the listing of media posts
 */
function displayMedia() {
  const section = document.querySelector( '.o__media' );
  if ( !section ) {
    return;
  }  

  const postsContainer = section.querySelector( '.o__media-posts' );
  const filterYear = section.querySelector( '.o__media-filters-year' );
  const filterMediaType = section.querySelector( '.o__media-filters-media-type' );
  const filterIcon = section.querySelector( '.o__media-filters-icon' );
  const searchInput = section.querySelector( '.o__media-filters-search-input' );
  const searchSubmitButton = section.querySelector( '.a__btn__search' );
  const loadMoreButton = section.querySelector( '.o__media-view-more-button' );
  const loader = section.querySelector( '.loader' );
  const loaderActiveClass = 'loader--active';
  const searchLabel = 'SEARCH';

  let pageToLoad = 1;
  let year = null;
  let mediaType = null;
  let searchText = null;
  let labelValue = '';

  const selectricOptions = {
    disableOnMobile: false,
    nativeOnMobile: false,
    arrowButtonMarkup: filterIcon.innerHTML,
    onOpen: ( element ) => {
      const parent = element.closest( '.selectric-wrapper' );
      const label = parent.querySelector( '.label' );
      const defaultOption = parent.querySelector( '.selectric-items li' );
      labelValue = label.innerHTML;
      label.innerHTML = defaultOption.innerHTML;
    },
    onClose: ( element ) => {
      const parent = element.closest( '.selectric-wrapper' );
      const label = parent.querySelector( '.label' );
      label.innerHTML = labelValue;
    },
    onChange: ( element ) => {
      const label = element.options[element.selectedIndex].value;
      if ( 'transactions_by_year' === element.id ) {
        year = label;
      } else {
        mediaType = label;
      }      
      filterPosts( pageToLoad, year, mediaType, searchText );      
    }
  };

  // Load select styling.
  $( filterYear ).selectric( selectricOptions );
  $( filterMediaType ).selectric( selectricOptions );  

  // Load initial posts.
  clearContainer();
  fetchPosts( pageToLoad );

  // Set Load More button event.
  loadMoreButton.addEventListener( 'click', () => {
    fetchPosts( pageToLoad, year, mediaType, searchText );
  });

  searchSubmitButton.addEventListener( 'click', () => {
    searchText = searchInput.value;
    if ( searchText === searchLabel ) {
      searchText = null;
    }
    filterPosts( pageToLoad, year, mediaType, searchText );
  });

  /**
   * Display posts on the page
   *
   * @param {number} page The pagination, page number
   * @param {string} year The filtered year
   * @param {string} mediaType The filtered media type
   * @param {string} searchText The searchText
   */
  function fetchPosts( page, year, mediaType, searchText ) {
    const apiURL = createApiUrl( page, year, mediaType, searchText );

    loader.classList.add( loaderActiveClass );
    loadMoreButton.classList.remove( 'active' );

    fetch( apiURL, window.madison.apiHeaders )
      .then( ( response ) => {
        if ( response.status === 200 ) {
          return response.json();
        } else {
          throw new Error( 'Error.' );
        }
      })
      .then( ( posts ) => {
        onFetchEnd();
        render( posts.data );
        loadMore( page, posts.max_pages );
      })
      .catch( () => {
        onFetchEnd();
        postsContainer.innerHTML = 'An error ocurred. Please try again by reloading the page.';
        postsContainer.classList.add( 'no-text' );
      });
  }

  /**
   * Dom elements configurations after the fetch finishes.
   */
  function onFetchEnd() {
    loader.classList.remove( loaderActiveClass );
  }

  /**
   * Creates the API url
   *
   * @param {number} page The pagination, page number
   * @param {string} year The filtered year
   * @param {string} mediaType The filtered media type
   * @param {string} searchText The searched text
   *
   * @return {string} api url
   */
  function createApiUrl( page, year, mediaType, searchText ) {
    window.madison = 'madison' in window ? window.madison : {};
    let apiURL = `${window.madison.api_url}posts?news_type=media&page=${page}`;

    if ( year && 'all' !== year ) {
      apiURL = `${apiURL}&year=${year}`;
    }
    if ( mediaType && 'all' !== mediaType ) {
      apiURL = `${apiURL}&media_type=${mediaType}`;
    }
    if ( searchText && '' !== searchText ) {
      apiURL = `${apiURL}&search_text=${searchText}`;
    }

    return apiURL;
  }

  /**
   * Clears container and shows posts.
   *
   * @param {number} page The pagination, page number
   * @param {string} year The filtered year
   * @param {string} mediaType The filtered media type
   * @param {string} searchText The search text
   */
  function filterPosts( page, year, mediaType, searchText ) {
    clearContainer();
    pageToLoad = 1;
    fetchPosts( pageToLoad, year, mediaType, searchText );
  }

  /**
   * Display transactions on the page
   *
   * @param {Array} posts Array of Posts in JSON
   */
  function render( posts ) {
    if ( posts.length > 0 ) {
      postsContainer.insertAdjacentHTML( 'beforeend', posts.join( ' ' ) );    
      postsContainer.classList.remove( 'no-text' );
    } else {
      postsContainer.innerHTML = 'No Media Posts Found.';
      postsContainer.classList.add( 'no-text' );
    }
  }

  /**
   * Clear the container.
   */
  function clearContainer() {
    postsContainer.innerHTML = '';
  }

  /**
   * Toggle the Load More button and save the next page to load variable.
   *
   * @param {number} actualPage The actual page
   * @param {number} totalPages The total pages of Posts
   */
  function loadMore( actualPage, totalPages ) {
    if ( actualPage < totalPages ) {
      loadMoreButton.classList.add( 'active' );
      const nextPage = pageToLoad + 1;
      if ( nextPage <= totalPages ) {
        pageToLoad = nextPage;
      }
    } else {
      loadMoreButton.classList.remove( 'active' );
    }
  }
}

module.exports = displayMedia;