<?php
use Lean\Load;

$args = wp_parse_args( $args, [
	'title' => '',
	'introduction' => '',
	'products' => [],
	'bg_color' => 'blue',
]);

if ( empty( $args['products'] ) ) {
	return;
}

Load::molecule( 'navigation/link-anchor', [
	'title' => $args['title'],
	'default' => 'products',
]);
?>

<section class="o__products o__products--<?php echo esc_attr( $args['bg_color'] ); ?>">
	<div class="container container--no-padding container--padding-mobile">

		<?php
		$theme = 'blue' === $args['bg_color'] ? 'light' : 'dark';

		Load::molecule( 'content/content', [
			'title' => $args['title'],
			'content' => $args['introduction'],
			'theme' => $theme,
			'aligned' => 'centered',
		]);

		Load::atom( 'separator/separator', [
			'color' => 'red',
		]);
		?>

		<div class="o__products-products">  

			<?php foreach ( $args['products'] as $product ) : ?>
				<div class="o__products-product">
					<h3 class="o__products-product-title fade"><?php echo wp_kses_post( $product['title'] ); ?></h3>
					<p class="o__products-product-description fade"><?php echo wp_kses_post( $product['description'] ); ?></p>
				</div>
			<?php endforeach; ?>

		</div>

	</div>
</section>
