<?php use Lean\Load; ?>

<section class="o__page">
	<div class="container container--no-padding container--padding-mobile">
	<?php while ( have_posts() ) :
		the_post();

		Load::atom( 'buttons/back-button' );

		Load::molecule( 'content/content', [
			'title' => get_the_title(),
			'theme' => 'dark',
			'aligned' => 'centered',
		]);
		?>
		
		<div class="o__page-content">
			<?php the_content(); ?>
		</div>

	<?php endwhile; ?>
	</div>
</section>
