<?php
use Lean\Load;

$args = wp_parse_args( $args, [
  'title' => '',
  'introduction' => '',
  'entry_points' => [],
  'bg_color' => 'blue',
]);

if ( empty( $args['entry_points'] ) ) {
	return;
}

Load::molecule( 'navigation/link-anchor', [
	'title' => $args['title'],
	'default' => 'history-timeline',
]);
?>

<section class="o__history-timeline o__history-timeline--<?php echo esc_attr( $args['bg_color'] ); ?>">

	<?php
	Load::atom( 'separator/separator', [
		'type' => 'large',
		'color' => 'red',
	]);
	?>

  <div class="container container--no-padding">

		<div class="o__history-timeline-subcontainer">
			<?php
			Load::molecule( 'content/content', [
				'title' => $args['title'],
				'content' => $args['introduction'],
				'theme' => 'blue' === $args['bg_color'] ? 'light' : 'dark',
			]);
			?>
			
			<div class="o__history-timeline-timeline owl-carousel owl-theme">
			<?php
			foreach ( $args['entry_points'] as $entry ) {
				Load::molecule( 'history-timeline-entry/history-timeline-entry', [
					'year' => $entry['year'],
					'image' => $entry['image'],
					'title' => $entry['title'],
					'description' => $entry['description'],
					'theme' => $args['bg_color'],
				]);
			}
			?>
			</div>
			<span class="o__history-timeline-slider-nav-icon"><?php use_icon( 'side-arrow-white' ); ?></span>

			<div class="o__history-timeline-years">
				<div class="o__history-timeline-years-subcontainer">
					<?php foreach ( $args['entry_points'] as $index => $entry ) :	?>
					<div class="o__history-timeline-year <?php if ( 0 === $index ) { echo 'active'; } ?>">
						<a class="o__history-timeline-year-text" href="#<?php echo esc_attr( $entry['year'] ); ?>">
							<?php echo esc_html( $entry['year'] ); ?>
						</a>
						<a class="o__history-timeline-year-point" href="#<?php echo esc_attr( $entry['year'] ); ?>"></a>
					</div>
					<?php endforeach; ?>
				</div>
			</div>

			<div class="o__history-timeline-mobile-nav">
				<span class="o__history-timeline-mobile-nav-current">1</span>
				<span>/</span>
				<span class="o__history-timeline-mobile-nav-total"><?php echo esc_html( count( $args['entry_points'] ) ); ?></span>
			</div>

		</div>

	</div>
</section>
