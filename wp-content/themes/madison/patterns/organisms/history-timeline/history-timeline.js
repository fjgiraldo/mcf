const from = require( 'array.from' );

/**
 * Starts the slider and the timeline animations.
 */
function historyTimeline() {
  const sectionContainer = document.querySelector( '.o__history-timeline' );

  if ( !sectionContainer ) {
    return;
  }

  const sliderContainer = sectionContainer.querySelector( '.o__history-timeline-timeline' );
  const slides = from( sliderContainer.querySelectorAll( '.m__history-timeline-entry' ) );
  const navIcon = sectionContainer.querySelector( '.o__history-timeline-slider-nav-icon' );
  const currentSlide = sectionContainer.querySelector( '.o__history-timeline-mobile-nav-current' );
  const yearsContainer = sectionContainer.querySelector( '.o__history-timeline-years-subcontainer' );
  const years = from( sectionContainer.querySelectorAll( '.o__history-timeline-year' ) );
  const activeClass = 'active';  
  const $sliderContainer = $( sliderContainer );

  initSlider();
  setActiveYearClass();

  /**
   * Initiates the slider.
   */
  function initSlider() {
    if ( slides.length > 0 ) {
      $sliderContainer.owlCarousel({
        items: 1,
        nav: true,
        dots: false,
        center: true,
        smartSpeed: 550,
        navText: [navIcon.innerHTML, navIcon.innerHTML],
        URLhashListener:true,
        startPosition: `#${years[0]}`,
        onChanged: onSlideChanged,
        responsive: {
          0: {
            items: 1,
            stagePadding: 50,
            margin: 20,
          },
          425: {
            items: 1,
            stagePadding: 80,
            margin: 90,
          },          
          1024: {
            items: 1,
            stagePadding: 80,
            margin: 90,            
          }
        }        
      });    
    }
  }

  /**
   * Sets the active class to the clicked year.
   */
  function setActiveYearClass() {
    if ( years.length <= 0 ) {
      return;
    }
    years.forEach( ( year, index ) => {
      year.addEventListener( 'click', () => { 
        activateYear( year, index );
      });
    });
  }

  /**
   * Sets the active class to the year when the next and previous slider buttons are clicked.
   *
   * @param {object} event - the changed data.
   */
  function onSlideChanged( event ) {
    let index = parseInt( event.item.index ); 
    index = isNaN( index ) ? 0 : index;
    const currentYear = years[index];
    if ( !currentYear ) {
      return;
    }
    activateYear( currentYear, index );
    currentSlide.innerHTML = index + 1;
  }  

  /**
   * Remove the active class to all years and sets the active class to the chosen year.
   *
   * @param {element} actualYear - the actual Year chosen.
   * @param {number} index - the actual Year position in the Years array.
   */
  function activateYear( actualYear, index ) {
    years.forEach( ( year ) => year.classList.remove( activeClass ) );
    actualYear.classList.add( activeClass );
    // Move timeline so actual year moves to first position.
    const distance = index * 110;
    yearsContainer.style.transform = `translateX(-${distance}px)`;
  }
}

module.exports = historyTimeline;