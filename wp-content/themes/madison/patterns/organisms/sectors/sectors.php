<?php
use Lean\Load;

$args = wp_parse_args( $args, [
	'title' => '',
	'subsectors' => [],
	'button' => '',
	'button_file_label' => 'Download One-Pager',
	'button_file' => '',
	'bg_color' => 'white',
]);

if ( empty( $args['subsectors'] ) ) {
	return;
}

Load::molecule( 'navigation/link-anchor', [
	'title' => $args['title'],
	'default' => 'sectors',
]);
?>

<section class="o__sectors o__sectors--<?php echo esc_attr( $args['bg_color'] ); ?>">
	<div class="container container--no-padding container--padding-mobile">
		<div class="o__sectors-box <?php if ( 1 < count( $args['subsectors'] ) ) : ?>o__sectors-box--large<?php endif; ?>">
			<h2 class="o__sectors-box-title"><?php echo esc_html( $args['title'] ); ?></h2>
			<div class="o__sectors-industry-areas">
				
					<?php if ( 1 === count( $args['subsectors'] ) ) : ?>

					<ul class="o__sectors-industry-areas-list">
						<?php foreach ( $args['subsectors'][0]['industry_areas'] as $industry_area ) : ?>
						<li class="o__sectors-industry-area"><?php echo esc_html( $industry_area['industry_area_name'] ); ?></li>
						<?php endforeach; ?>
					</ul>

					<?php elseif ( ! empty( $args['subsectors'] ) ) : ?>

					<span class="o__sectors-select-icon"><?php use_icon( 'side-arrow-lightblue' ); ?></span>

					<div class="o__sectors-select-helper">Select from the list below.</div>

					<select class="o__sectors-select" name="subsectors" id="subsectors">
						<?php foreach ( $args['subsectors'] as $index => $subsector ) : ?>
						<option value="<?php echo esc_attr( $index ); ?>"><?php echo esc_html( $subsector['subsector_name'] ); ?></option>
						<?php endforeach; ?>
					</select>
					
					<?php foreach ( $args['subsectors'] as $index => $industry_areas ) : ?>
						<ul class="o__sectors-industry-areas-list <?php if ( 0 !== $index ) : ?>hidden<?php endif; ?>">
						<?php foreach ( $industry_areas['industry_areas'] as $industry_area ) : ?>
						<li class="o__sectors-industry-area"><?php echo esc_html( $industry_area['industry_area_name'] ); ?></li>
						<?php endforeach; ?>
						</ul>
					<?php endforeach; ?>
					
					<?php endif; ?>
				
			</div>
		</div>

		<?php
		$button_type = 'blue' === $args['bg_color'] ? 'primary' : 'secondary';

		Load::atom( 'buttons/link-as-button', [
			'link' => $args['button'],
			'type' => $button_type,
		]);

		Load::atom( 'buttons/file-button', [
			'label' => $args['button_file_label'],
			'file' => $args['button_file'],
			'type' => $button_type,
		]);
		?>

	</div>
</section>
