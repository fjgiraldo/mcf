const from = require( 'array.from' );
const $ = require( 'jquery' );

/**
 * Starts the slider and the timeline animations.
 */
function selectIndustryAreas() {
  const sectorsSection = document.querySelector( '.o__sectors' );
  if ( !sectorsSection ) {
    return;
  }  

  const selectIndustryArea = sectorsSection.querySelector( '.o__sectors-select' );
  if ( !selectIndustryArea ) {
    return;
  }  

  const industryAreasSections = from( sectorsSection.querySelectorAll( '.o__sectors-industry-areas-list' ) );
  const selectIcon = sectorsSection.querySelector( '.o__sectors-select-icon' );
  let icon = '';

  if ( selectIcon ) {
    icon = selectIcon.innerHTML;
  }

  const selectricOptions = {
    disableOnMobile: false,
    nativeOnMobile: false,
    arrowButtonMarkup: icon,
    onChange: ( element ) => {   
      showIndustryArea( element.options[ element.selectedIndex ].value );      
    }
  };

  $( selectIndustryArea ).selectric( selectricOptions );

  /**
   * Hides the Industry Areas boxes and shows the chosen by the Select
   *
   * @param {number} selectedIndex The index of the selected industry area section
   */
  function showIndustryArea( selectedIndex ) {
    industryAreasSections.forEach( ( section ) => section.classList.add( 'hidden' ) );
    industryAreasSections[ selectedIndex ].classList.remove( 'hidden' );
  }
}

module.exports = selectIndustryAreas;