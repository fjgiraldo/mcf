<?php
use Lean\Load;
use Madison\Modules\Transactions\Transactions as Transaction;

$args = wp_parse_args( $args, [
	'title' => '',
	'boxes' => [],
	'button' => [],
]);

if ( empty( $args['boxes'] ) ) {
	return;
}

Load::molecule( 'navigation/link-anchor', [
	'title' => $args['title'],
	'default' => 'transactions-highlights',
]);
?>

<section class="o__transactions-highlights">
	<div class="container container--no-padding">

		<?php if ( $args['title'] ) : ?>
		<h2 class="o__transactions-highlights-title fade">
			<?php echo esc_html( $args['title'] ); ?>
		</h2>	
		<?php endif; ?>

		<div class="o__transactions-highlights-boxes owl-carousel owl-theme">
		<?php
		foreach ( $args['boxes'] as $box ) {
			Load::molecule( 'box/transactions-highlights-box', Transaction::get_data( $box ) );
		}
		?>
		</div>	

		<span class="o__transactions-highlights-slider-nav-icon"><?php use_icon( 'side-arrow-lightblue' ); ?></span>

		<?php
		if ( ! empty( $args['button'] ) ) {
			Load::atom( 'buttons/link-as-button', [
				'link' => $args['button'],
				'type' => 'secondary',
			]);
		}
		?>

	</div>
</section>
