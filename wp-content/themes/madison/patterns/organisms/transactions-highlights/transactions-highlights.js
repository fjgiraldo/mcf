const from = require( 'array.from' );

/**
 * Function that activates the Transactions Highlights slider.
 */
function transactionsHighlightsSlider() {
  const sectionContainer = document.querySelector( '.o__transactions-highlights' );

  if ( !sectionContainer ) {
    return;
  }

  const sliderContainer = sectionContainer.querySelector( '.o__transactions-highlights-boxes' );
  const navIcon = sectionContainer.querySelector( '.o__transactions-highlights-slider-nav-icon' );
  const slides = from( sliderContainer.querySelectorAll( '.m__transactions-highlights-box' ) );
  const $sliderContainer = $( sliderContainer );

  initSlider();
  
  /**
   * Initiates the slider.
   */
  function initSlider() {
    if ( slides.length > 0 ) {
      $sliderContainer.owlCarousel({
        nav: true,
        navText: [navIcon.innerHTML, navIcon.innerHTML],
        smartSpeed: 550,
        pullDrag: false,
        responsive: {
          0: {
            items: 1,
            stagePadding: 64,
            margin: 32,
          },
          425: {
            items: 2,
            stagePadding: 77,
            margin: 77,
          },
          1024: {
            items: 3,
            stagePadding: 18,
            margin: 18,            
          }
        }
      });
    }
  }
}

export default transactionsHighlightsSlider;