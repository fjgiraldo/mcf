<?php
use Lean\Load;
use Madison\Modules\DealAnnouncements\DealAnnouncements as DealAnnouncement;

$args = wp_parse_args( $args, [
	'title' => '',
	'introduction' => '',
	'vertical' => null,
	'bg_color' => 'blue',
]);

Load::molecule( 'navigation/link-anchor', [
	'title' => $args['title'],
	'default' => 'deal-announcements',
]);
?>

<section class="o__deal-announcements o__deal-announcements--<?php echo esc_attr( $args['bg_color'] ); ?>">

	<div class="container container--no-padding container--padding-mobile">

		<?php
		$theme = 'blue' === $args['bg_color'] ? 'light' : 'dark';

		Load::molecule( 'content/content', [
			'title' => $args['title'],
			'content' => $args['introduction'],
			'theme' => $theme,
		]);
		?> 

		<div class="o__deal-announcements-posts">
			<?php
			$args = [
				'posts' => DealAnnouncement::get_posts( $args['vertical'] ),
			];
			Load::molecule( 'list/two-col-list', $args );
			?>
		</div>

	</div>
</section>
