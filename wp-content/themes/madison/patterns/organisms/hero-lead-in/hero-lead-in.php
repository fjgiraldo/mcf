<?php
use Lean\Load;

$args = wp_parse_args( $args, [
	'title' => '',
	'image' => '',
	'content' => '',
	'button' => '',
	'bg_color' => 'white',
	'box_color' => 'red',
	'box_position' => 'center',
	'separation_bar' => 'false',
]);

if ( empty( $args['image'] ) || empty( $args['content'] ) ) {
	return;
}

Load::molecule( 'navigation/link-anchor', [
	'title' => $args['title'],
	'default' => 'hero-lead-in',
]);
?>

<section class="o__hero-lead-in o__hero-lead-in--<?php echo esc_attr( $args['bg_color'] ); ?>">
	<div class="container container--no-padding">
		
		<div class="o__hero-lead-in-subcontainer">

			<?php
			$image_args = [
				'alt' => $args['title'],
				'class' => 'o__hero-lead-in-image',
			];
			echo wp_get_attachment_image( $args['image'], 'full', false, $image_args );
			?>

			<div class="o__hero-lead-in-box o__hero-lead-in-box--<?php echo esc_attr( $args['box_color'] ); ?> o__hero-lead-in-box--<?php echo esc_attr( $args['box_position'] ); ?>">

				<h2 class="o__hero-lead-in-title fade">
					<?php echo esc_html( $args['title'] ); ?>
				</h2>	

			</div>

		</div>

		<?php if ( $args['content'] ) : ?>
		<div class="o__hero-lead-in-content o__hero-lead-in-content--<?php echo esc_attr( $args['box_position'] ); ?> fade">
			<?php echo wp_kses_post( $args['content'] ); ?>
		</div>
		<?php endif; ?>

		<?php if ( ! empty( $args['button']['url'] ) ) : ?>
		<div class="o__hero-lead-in-button">
		<?php
		Load::atom( 'buttons/link-as-button', [
			'link' => $args['button'],
			'type' => 'blue' === $args['bg_color'] ? 'primary' : 'secondary',
		]);
		?>
		</div>
		<?php endif; ?>

		<?php
		if ( $args['separation_bar'] ) {
			Load::atom( 'separator/separator', [
				'type' => 'section',
				'color' => 'blue',
			]);
		}
		?>

	</div>
</section>
