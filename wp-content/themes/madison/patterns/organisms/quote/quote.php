<?php
use Lean\Load;
use Madison\Helpers;

$args = wp_parse_args( $args, [
	'quotes' => [],
]);

if ( empty( $args['quotes'] ) ) {
	return;
}

Load::molecule( 'navigation/link-anchor', [
	'title' => '',
	'default' => 'quote',
]);
?>

<section class="o__quote">

	<?php foreach ( $args['quotes'] as $index => $quote ) : ?>
		<?php $row = 0 === $index % 2 ? 'odd' : 'even'; ?>
		<div class="o__quote-quote-container">
			<div class="o__quote-image-box">
				<?php Helpers::bg_image( $quote['image'], '.o__quote-image-container-index-' . $index ); ?>
				<figure class="o__quote-image-container o__quote-image-container-index-<?php echo esc_attr( $index ); ?>">
					<?php if ( $quote['image_caption'] ) : ?>
					<figcaption class="o__quote-image-caption o__quote-image-caption--<?php echo esc_attr( $row ); ?>">
						<?php echo esc_html( $quote['image_caption'] ); ?>
					</figcaption>
					<?php endif; ?>
				</figure>
			</div>
			<div class="o__quote-text-box">
				<div class="o__quote-text o__quote-text--<?php echo esc_attr( $row ); ?>">
					<?php echo esc_html( $quote['text'] ); ?>
					<div class="o__quote-text-line"></div>
				</div>
			</div>
		</div>
	<?php endforeach; ?>

</section>
