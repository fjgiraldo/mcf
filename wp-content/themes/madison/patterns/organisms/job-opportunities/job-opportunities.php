<?php
use Lean\Load;
use Madison\Modules\JobOpportunities\JobOpportunities as JobOpportunity;

$args = wp_parse_args( $args, [
	'title' => '',
	'introduction' => '',
	'bg_color' => 'blue',
]);

Load::molecule( 'navigation/link-anchor', [
	'title' => $args['title'],
	'default' => 'job-opportunities',
]);
?>

<section class="o__job-opportunities o__job-opportunities--<?php echo esc_attr( $args['bg_color'] ); ?>">

	<div class="container container--no-padding container--padding-mobile">

		<?php
		$theme = 'blue' === $args['bg_color'] ? 'light' : 'dark';

		Load::molecule( 'content/content', [
			'title' => $args['title'],
			'content' => $args['introduction'],
			'theme' => $theme,
		]);
		?> 

		<div class="o__job-opportunities-posts">
			<?php
			$args = [
				'posts' => JobOpportunity::get_posts(),
			];
			Load::molecule( 'list/two-col-list', $args );
			?>
		</div>

	</div>
</section>
