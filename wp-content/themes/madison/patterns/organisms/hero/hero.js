const breakpoints = require( 'atoms/base/breakpoints' );
/**
 * Function that send the user to the next section when clicking the next icon on the Hero.
 */
function hero() {
  const hero = document.querySelector( '.o__hero' );

  if ( !hero ) {
    return;
  }

  // the second next sibling because the first one is the anchoring div.
  const heroNextSection = document.querySelector( '.o__hero' ).nextElementSibling.nextElementSibling;

  if ( !heroNextSection ) {
    return;
  }

  const nextIcon = hero.querySelector( '.o__hero-next-icon' );
  
  if ( !nextIcon ) {
    return;
  }

  const $body = $( 'body, html' );
  const $heroNextSection = $( heroNextSection );
  let offset = 0;

  calculateOffset();

  nextIcon.addEventListener( 'click', onIconClick );

  window.addEventListener( 'resize', onResize );

  /**
   * On resize functions.
   */
  function onResize() {
    calculateOffset();
  }

  /**
   * Function that calculates the offset for different devices.
   * The offset is necessary due to the fixed header.
   */
  function calculateOffset() {
    if ( window.innerWidth > breakpoints.tablet ) {
      offset = 80;
    } else {
      offset = 40;
    }
  }

  /**
   * Function that is called when the next icon is clicked.
   */
  function onIconClick() {
    $body.animate({
      scrollTop: $heroNextSection.offset().top - offset
    }, 1000 );
  }  
}

export default hero;