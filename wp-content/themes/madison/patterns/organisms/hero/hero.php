<?php
use Lean\Load;
use Madison\Helpers;

$args = wp_parse_args( $args, [
	'is_visible' => false,
	'title' => '',
	'content' => '',
	'bg_image' => '',
	'box_color' => 'blue',
]);

if ( ! $args['is_visible'] ) {
	return;
}

Load::molecule( 'navigation/link-anchor', [
	'title' => $args['title'],
	'default' => 'hero',
]);

Helpers::bg_image( $args['bg_image'], '.o__hero' );
?>

<section class="o__hero">
	<div class="container">
		<?php if ( $args['title'] || $args['content'] ) : ?>
		<div class="o__hero-box<?php if ( 'blue' !== $args['box_color'] ) : ?> o__hero-box--red<?php endif; ?>">
			<h2 class="o__hero-title"><?php echo esc_html( $args['title'] ); ?></h2>
			<div class="o__hero-content"><?php echo esc_html( $args['content'] ); ?></div>
		</div>
		<?php endif; ?>
		<?php
		Load::atom( 'icons/circular-arrow-icon', [
			'orientation' => 'down',
			'class' => 'o__hero-next-icon',
		]);
		?>
	</div>
</section>
