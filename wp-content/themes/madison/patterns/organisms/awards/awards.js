const from = require( 'array.from' );

/**
 * Function that activates the Awards slider.
 */
function awardsSlider() {
  const sectionContainer = document.querySelector( '.o__awards' );

  if ( !sectionContainer ) {
    return;
  }

  const sliderContainer = sectionContainer.querySelector( '.o__awards-boxes' );
  const navIcon = sectionContainer.querySelector( '.o__awards-slider-nav-icon' );
  const slides = from( sliderContainer.querySelectorAll( '.m__award-box' ) );
  const $sliderContainer = $( sliderContainer );
  let navIconHTML = ['&#x27;next&#x27;', '&#x27;prev&#x27;'];
  if ( navIcon ) {
    navIconHTML = [navIcon.innerHTML, navIcon.innerHTML];
  }

  if ( slides.length > 3 ) {
    $sliderContainer.owlCarousel({
      nav: true,
      navText: navIconHTML,
      smartSpeed: 550,
      responsive: {
        0: {
          items: 1,
          stagePadding: 64,
          margin: 32,
        },
        425: {
          items: 1,
          stagePadding: 100,
          margin: 250,
        },        
        768: {
          items: 2,
          stagePadding: 77,
          margin: 77,
        },
        1024: {
          items: 3,
          stagePadding: 18,
          margin: 18,            
        }
      }
    });
  } else {
    sliderContainer.classList.add( 'no-carousel' );
  }
}

export default awardsSlider;