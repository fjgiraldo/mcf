<?php
use Lean\Load;

$args = wp_parse_args( $args, [
	'title' => '',
	'boxes' => [],
	'button' => [],
	'bg_color' => 'white',
]);

if ( empty( $args['boxes'] ) ) {
	return;
}

Load::molecule( 'navigation/link-anchor', [
	'title' => $args['title'],
	'default' => 'awards',
]);
?>

<section class="o__awards o__awards--<?php echo esc_attr( $args['bg_color'] ); ?>">
	<div class="container container--no-padding">

		<?php
		Load::molecule( 'content/content', [
			'title' => $args['title'],
			'theme' => 'blue' === $args['bg_color'] ? 'light' : 'dark',
			'aligned' => 'centered',
		]);
		?>		

		<div class="o__awards-boxes owl-carousel owl-theme">
		<?php
		foreach ( $args['boxes'] as $box ) {
			Load::molecule( 'box/award-box', $box );
		}
		?>
		</div>  

		<span class="o__awards-slider-nav-icon"><?php use_icon( 'side-arrow-lightblue' ); ?></span>

		<?php
		Load::atom( 'buttons/link-as-button', [
			'link' => $args['button'],
			'type' => 'blue' === $args['bg_color'] ? 'primary' : 'secondary',
		]);
		?>

	</div>
</section>
