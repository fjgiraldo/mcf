<?php
use Lean\Load;

$args = wp_parse_args( $args, [
	'title' => '',
	'image' => '',
	'content' => '',
	'button' => '',
	'bg_color' => 'white',
	'box_color' => 'red',
]);

if ( empty( $args['image'] ) || empty( $args['content'] ) ) {
	return;
}

Load::molecule( 'navigation/link-anchor', [
	'title' => $args['title'],
	'default' => 'description',
]);
?>

<section class="o__description o__description--<?php echo esc_attr( $args['bg_color'] ); ?>">
	<div class="container container--no-padding">
		<?php if ( $args['title'] ) : ?>
		<h2 class="o__description-title fade">
			<?php echo esc_html( $args['title'] ); ?>
		</h2>	
		<?php endif; ?>
		<div class="o__description-subcontainer">

			<?php
			$image_args = [
				'alt' => $args['title'],
				'class' => 'o__description-image',
			];
			echo wp_get_attachment_image( $args['image'], 'full', false, $image_args );
			?>

			<div class="o__description-box o__description-box--<?php echo esc_attr( $args['box_color'] ); ?> <?php if ( empty( $args['button']['url'] ) ) : ?>o__description-box--only-text<?php endif; ?>">

				<div class="o__description-content fade">
					<?php echo esc_html( $args['content'] ); ?>
				</div>

				<?php if ( ! empty( $args['button']['url'] ) ) : ?>
				<div class="o__description-button">
				<?php
				Load::atom( 'buttons/link-as-button', [
					'link' => $args['button'],
					'type' => 'primary',
				]);
				?>
				</div>
				<?php endif; ?>

			</div>

		</div>
	</div>
</section>
