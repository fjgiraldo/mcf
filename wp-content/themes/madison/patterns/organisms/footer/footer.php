<?php use Lean\Load; ?>

<footer
	class="o__footer"
	role="contentinfo"
	itemscope
	itemtype="http://schema.org/WPFooter">

  <?php
  Load::organism( 'footer/footer-top' );
  Load::organism( 'footer/footer-bottom' );
  ?>	

</footer>
