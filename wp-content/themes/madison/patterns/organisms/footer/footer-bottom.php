<?php
use Lean\Load;

$footer_copyright_text = get_field( 'footer_copyright_text', 'options' );
$copyright = str_replace( '%YEAR%', date( 'Y' ), $footer_copyright_text );

$footnote_options = ( get_the_id() ) ? get_field( 'footnotes_options', get_the_id() ) : 'none';
$footnote_global_text = get_field( 'footnote_text', 'options' );
$footnote_specific_text = ( get_the_id() ) ? get_field( 'specific_footnotes', get_the_id() ) : '';

switch ( $footnote_options ) {
	case 'global':
		$footnote_text = $footnote_global_text;
		break;
	case 'specific':
		$footnote_text = $footnote_specific_text;
		break;
	default:
		$footnote_text = '';
		break;
}
?>

<div class="o__footer-bottom">

	<?php if ( $footnote_text ) : ?>
	<div class="o__footer-bottom-container-top container container--no-padding container--padding-mobile">
		<div class="o__footer-bottom-footnote">
			<?php echo wp_kses_post( $footnote_text ); ?>
		</div>
	</div>
	<?php endif; ?>

  <div class="o__footer-bottom-container-bottom container container--no-padding container--padding-mobile">
		<div class="o__footer-bottom-subcontainer">
			<div class="o__footer-bottom-copyright">
				<?php echo esc_html( $copyright ); ?>
			</div>
			<div class="o__footer-bottom-menu">
				<?php Load::molecule( 'navigation/footer-nav' ); ?>		
		  </div>
		</div>
	</div>  

</div>
