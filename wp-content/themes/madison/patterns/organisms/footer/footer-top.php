<?php
use Lean\Load;

$logo = get_field( 'footer_logo', 'options' );
$contact_text = get_field( 'footer_contact_us_text', 'options' );
$social_media_title = get_field( 'social_media_title', 'options' );
$social_media_networks = get_field( 'social_media_networks', 'options' );
$social_media_networks_new_window = get_field( 'social_media_new_window', 'options' );
$mailing_list_text = get_field( 'mailing_list_text', 'options' );
$mailing_list_link = get_field( 'mailing_list_link', 'options' );
?>

<div class="o__footer-top">
	<div class="container container--no-padding">

		<?php Load::atom( 'buttons/back-top' ); ?>

		<div class="o__footer-top-col-left">

			<?php if ( $logo ) : ?>
			<a 
				class="o__footer-top-logo"
				itemprop="mainEntityOfPage" 
				href="<?php bloginfo( 'url' ); ?>" 
				title="<?php bloginfo( 'name' ); ?>" >
				<img src="<?php echo esc_url( $logo ); ?>" alt="<?php bloginfo( 'name' ); ?>" />
			</a>
			<?php endif; ?>

			<div class="o__footer-top-contact-text">
				<?php echo wp_kses_post( $contact_text ); ?>
			</div>

			<div class="o__footer-top-social-media">
				<h3 class="o__footer-top-social-media-title"><?php echo esc_html( $social_media_title ); ?></h3>

				<?php foreach ( $social_media_networks as $network ) : ?>
					<a 
						class="o__footer-top-social-media-link"
						<?php if ( $social_media_networks_new_window ) : ?>target="_blank"<?php endif; ?> 
						href="<?php echo esc_url( $network['link'] ); ?>">
							<?php use_icon( $network['general_options_network'], 'o__footer-top-social-media-icon' ); ?>
							<?php use_icon( $network['general_options_network'] . '-hover', 'o__footer-top-social-media-icon-hover' ); ?>
						</a>
				<?php endforeach; ?>        
			</div>

		</div>

	</div>    

	<div class="o__footer-top-col-right">
		<div class="o__footer-top-col-right-container">
			<?php echo wp_kses_post( $mailing_list_text ); ?>

			<?php
			Load::atom( 'buttons/link-as-button', [
			  'link' => $mailing_list_link,
			  'type' => 'primary',
			]);
			?>
		</div>
	</div>

</div>
