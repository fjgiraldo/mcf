/**
 * Initialize the listing of insight posts
 */
function displayInsights() {
  const section = document.querySelector( '.o__insights' );
  if ( !section ) {
    return;
  }  

  const postsContainer = section.querySelector( '.o__insights-posts' );
  const loadMoreButton = section.querySelector( '.o__insights-view-more-button' );
  const loader = section.querySelector( '.loader' );
  const loaderActiveClass = 'loader--active';

  let pageToLoad = 1;

  // Load initial posts.
  clearContainer();
  fetchPosts( pageToLoad );

  // Set Load More button event.
  loadMoreButton.addEventListener( 'click', () => {
    fetchPosts( pageToLoad );
  });

  /**
   * Display posts on the page
   *
   * @param {number} page The pagination, page number
   */
  function fetchPosts( page ) {
    loader.classList.add( loaderActiveClass );
    loadMoreButton.classList.remove( 'active' );

    window.madison = 'madison' in window ? window.madison : {};
    const apiURL = `${window.madison.api_url}posts?news_type=insight&page=${page}`;

    fetch( apiURL, window.madison.apiHeaders )
      .then( ( response ) => {
        if ( response.status === 200 ) {
          return response.json();
        } else {
          throw new Error( 'Error.' );
        }
      })
      .then( ( posts ) => {
        onFetchEnd();
        render( posts.data );
        loadMore( page, posts.max_pages );
      })
      .catch( () => {
        onFetchEnd();
        postsContainer.innerHTML = 'An error ocurred. Please try again by reloading the page.';
        postsContainer.classList.add( 'no-text' );
      });
  }

  /**
   * Dom elements configurations after the fetch finishes.
   */
  function onFetchEnd() {
    loader.classList.remove( loaderActiveClass );
  }

  /**
   * Display transactions on the page
   *
   * @param {Array} posts Array of Posts in JSON
   */
  function render( posts ) {
    if ( posts.length > 0 ) {
      postsContainer.insertAdjacentHTML( 'beforeend', posts.join( ' ' ) );    
      postsContainer.classList.remove( 'no-text' );
    } else {
      postsContainer.innerHTML = 'No Insight Posts Found.';
      postsContainer.classList.add( 'no-text' );
    }
  }

  /**
   * Clear the container.
   */
  function clearContainer() {
    postsContainer.innerHTML = '';
  }

  /**
   * Toggle the Load More button and save the next page to load variable.
   *
   * @param {number} actualPage The actual page
   * @param {number} totalPages The total pages of Posts
   */
  function loadMore( actualPage, totalPages ) {
    if ( actualPage < totalPages ) {
      loadMoreButton.classList.add( 'active' );
      const nextPage = pageToLoad + 1;
      if ( nextPage <= totalPages ) {
        pageToLoad = nextPage;
      }
    } else {
      loadMoreButton.classList.remove( 'active' );
    }
  }
}

module.exports = displayInsights;