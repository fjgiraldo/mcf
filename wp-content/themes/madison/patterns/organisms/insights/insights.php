<?php
use Lean\Load;
use Madison\Modules\Posts\Posts;

$args = wp_parse_args( $args, [
	'title' => '',
	'introduction' => '',
	'bg_color' => 'blue',
]);

Load::molecule( 'navigation/link-anchor', [
	'title' => $args['title'],
	'default' => 'insights',
]);
?>

<section class="o__insights o__insights--<?php echo esc_attr( $args['bg_color'] ); ?>">

	<div class="container container--no-padding container--padding-mobile">

		<?php
		Load::atom( 'separator/separator', [
			'type' => 'large',
			'color' => 'blue',
		]);

		Load::molecule( 'content/content', [
			'title' => $args['title'],
			'content' => $args['introduction'],
			'theme' => 'blue' === $args['bg_color'] ? 'light' : 'dark',
			'aligned' => 'centered',
		]);
		?> 

		<div class="o__insights-posts">
		</div>

		<div class="loader">
			<div class="loader-animation"></div>
		</div>		

		<div class="container o__insights-button-container">
		<?php
		Load::atom( 'buttons/button', [
			'label' => 'More',
			'type' => 'secondary',
			'class' => 'o__insights-view-more-button',
		]);
		?>
		</div>	

	</div>
</section>
