<?php
use Lean\Load;

$args = wp_parse_args( $args, [
	'title' => '',
	'boxes' => [],
	'bg_color' => 'blue',
]);

if ( empty( $args['boxes'] ) ) {
	return;
}

Load::molecule( 'navigation/link-anchor', [
	'title' => $args['title'],
	'default' => 'facts-figures',
]);
?>

<section class="o__facts-figures o__facts-figures--<?php echo esc_attr( $args['bg_color'] ); ?>">
	<div class="container container--no-padding-tablet">

		<?php if ( $args['title'] ) : ?>
		<h2 class="o__facts-figures-title">
			<?php echo esc_html( $args['title'] ); ?>
		</h2>	
		<?php endif; ?>

		<div class="o__facts-figures-boxes">
		<?php
		foreach ( $args['boxes'] as $box ) {
			Load::molecule( 'box/facts-figures-box', $box );
		}
		?>
		</div>	

	</div>
</section>
