const from = require( 'array.from' );
const ScrollMagic = require( 'scrollmagic' );
const CountUp = require( 'countup.js' );

/**
 * Function that enables the number animations count up an all Fact boxes.
 */
function animateNumbers() {

  const numbersSection = document.querySelector( '.o__facts-figures' );

  if ( !numbersSection ) {
    return;
  }

  const numbersContainers = from( numbersSection.querySelectorAll( '.m__facts-figures-box-data' ) );
  const numbers = from( numbersSection.querySelectorAll( '.m__facts-figures-box-number' ) );
  const numbersController = [];

  if ( numbers.length > 0 ) {
    setNumbersCountUp();

    // Adds the scroll magic scenes to all number boxes.
    numbersContainers.forEach( addScrollingScene );    
  }

  /**
   * Function that adds the scroll magic scenes to the numbers.
   * 
   * @param {element} numberContainer the element to fade
   */
  function addScrollingScene( numberContainer ) {
    new ScrollMagic.Scene({ triggerElement: numberContainer, triggerHook: 0.55, duration: numbersSection.clientHeight })
      .addTo( window.madison.controller )
      .on( 'enter leave', function startNumbersAnimation( event ) {
        if ( event.type === 'enter' ) {
          numberContainer.classList.add( 'fade-animation' );
          startCountUp();
        } else if ( event.target.controller().info( 'scrollDirection' ) === 'REVERSE' ) {
          // Reset counter only when the user scrolls back above the module.
          numberContainer.classList.remove( 'fade-animation' );
          setTimeout( resetCountUp, 1000 );
        }
      });    
  }

  /**
   * Function that initializes the Count ups for all Facts boxes.
   */
  function setNumbersCountUp() {
    const initialNumber = 0;
    const duration = 2.5;

    const options = {
      decimal: '.',
    };
    numbers.forEach( function loopNumbers( element ) {
      const endNumber = element.dataset.endnumber;
      const decimals = element.dataset.decimals;
      numbersController.push( new CountUp( element, initialNumber, endNumber, decimals, duration, options ) );
    });    
  }

  /**
   * Function that starts the Count up of all Facts boxes.
   */
  function startCountUp() {
    numbersController.forEach( ( element ) => element.start() );
  }

  /**
   * Function that resets the Count up of all Facts boxes.
   */
  function resetCountUp() {
    numbersController.forEach( ( element ) => element.reset() );
  }  
}

export default animateNumbers;
