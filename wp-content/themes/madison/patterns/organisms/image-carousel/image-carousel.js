const breakpoints = require( 'atoms/base/breakpoints' );
const from = require( 'array.from' );

/**
 * Starts the image carousel.
 */
function imageCarousel() {
  const sectionContainer = document.querySelector( '.o__image-carousel' );

  if ( !sectionContainer ) {
    return;
  }

  const sliderContainer = sectionContainer.querySelector( '.o__image-carousel-slider' );
  const slides = from( sectionContainer.querySelectorAll( '.o__image-carousel-slide' ) );
  const navIcon = sectionContainer.querySelector( '.o__image-carousel-nav-icon' );
  let sliderNavIcons = '';

  if ( navIcon ) {
    sliderNavIcons = [navIcon.innerHTML, navIcon.innerHTML];
  }

  initSlider();

  /**
   * Initiates the slider.
   */
  function initSlider() {
    let stagePadding = 0;

    if ( window.innerWidth < breakpoints.mobile ) {
      // Hack for getting the image full width on mobile.
      stagePadding = -2;
    }

    if ( slides.length > 0 ) {
      $( sliderContainer ).owlCarousel({
        items: 1,
        nav: true,
        center: true,
        smartSpeed: 550,
        stagePadding: stagePadding,
        navText: sliderNavIcons,
        navContainer: '.o__image-carousel .carousel-custom-nav',
        dotsContainer: '.o__image-carousel .carousel-custom-dots'
      });    
    }
  }
}

module.exports = imageCarousel;