<?php
use Lean\Load;
use Madison\Helpers;

$args = wp_parse_args( $args, [
	'title' => '',
	'description' => '',
	'slides' => [],
	'bg_color' => 'blue',
]);

if ( empty( $args['slides'] ) ) {
	return;
}

Load::molecule( 'navigation/link-anchor', [
	'title' => $args['title'],
	'default' => 'image-carousel',
]);
?>

<section class="o__image-carousel o__image-carousel--<?php echo esc_attr( $args['bg_color'] ); ?>">

	<div class="o__image-carousel-slider-container">

		<div class="o__image-carousel-title-box">
			<h2><?php echo esc_html( $args['title'] ); ?></h2>
			<ul class='carousel-custom-dots owl-dots'>
			</ul>
			<div class="carousel-custom-nav">
			</div>
		</div>

		<div class="o__image-carousel-slider owl-carousel owl-theme">
			<?php foreach ( $args['slides'] as $index => $slide ) : ?>
				<div class="o__image-carousel-slide o__image-carousel-slide-index-<?php echo esc_attr( $index ); ?>">
					<?php Helpers::bg_image( $slide['image'], '.o__image-carousel-slide-index-' . $index ); ?>
					<?php if ( $slide['caption'] ) : ?>
					<div class="o__image-carousel-slide-caption"><?php echo esc_html( $slide['caption'] ); ?></div>
					<?php endif; ?>
				</div>
			<?php endforeach; ?>
		</div>

		<span class="o__image-carousel-nav-icon"><?php use_icon( 'side-arrow-lightblue' ); ?></span>

	</div>

	<div class="o__image-carousel-description container">
		<?php
		Load::molecule( 'content/content', [
			'content' => $args['description'],
			'aligned' => 'centered',
			'theme' => 'blue' === $args['bg_color'] ? 'light' : 'dark',
		]);
		?>
	</div>

</section>
