<?php
use Lean\Load;

$args = wp_parse_args( $args, [
  'title' => '',
  'title_2nd_style' => false,
  'content' => '',
  'excerpt' => '',
  'date' => false,
  'location' => '',
  'type' => 'text', // [ text, link, file ].
  'media_type' => '',
  'image' => '',
  'video' => '',
  'link_url' => '',
  'video_brightcove' => '',
  'video_image' => '',
	'show_share_buttons' => true,
	'apply_label' => 'Apply',
	'apply_link' => '',
]);
?>

<section class="o__post <?php if ( $args['title_2nd_style'] ) : ?>o__post--has-title-small<?php endif; ?>">

	<?php Load::atom( 'buttons/back-button' ); ?>
	
	<?php if ( ! $args['title_2nd_style'] ) : ?>
	<h1 class="o__post-title"><?php echo esc_html( $args['title'] ); ?></h1>
	<?php endif; ?>

	<?php
	Load::atoms( 'media/media', [
		'title' => $args['title'],
		'type' => $args['media_type'],
		'image' => $args['image'],
		'video' => $args['video'],
		'video_brightcove' => $args['video_brightcove'],
		'video_image' => $args['video_image'],
	]);

	if ( $args['show_share_buttons'] ) {
		Load::molecule( 'share/share-buttons' );
	}
	?>

	<div class="o__post-header">
		<?php if ( $args['date'] ) : ?>
		<div class="o__post-header-item o__post-header-date"><?php echo esc_html( $args['date'] ); ?></div>
		<?php endif; ?>
		<?php if ( $args['location'] ) : ?>
		<div class="o__post-header-item o__post-header-location">Location: <?php echo esc_html( $args['location'] ); ?></div>
		<?php endif; ?>
	</div>

	<?php if ( $args['title_2nd_style'] ) : ?>
	<h1 class="o__post-title o__post-title--small"><?php echo esc_html( $args['title'] ); ?></h1>
	<?php endif; ?>

	<div class="o__post-content">
		<?php
		if ( 'text' === $args['type'] ) :
			echo wp_kses_post( wpautop( $args['content'] ) );
		else :
			echo esc_html( $args['excerpt'] );
		?>
			<div class="o__post-link">
				<a href="<?php echo esc_url( $args['link_url'] ) ?>" target="_blank">
					<?php if ( 'file' === $args['type'] ) : ?>View PDF here.<?php else : ?>Visit link here.<?php endif; ?>
				</a>
			</div>
		<?php endif; ?>
	</div>

	<?php
	if ( $args['location'] && $args['apply_link'] ) {
		Load::atom( 'buttons/link-as-button', [
			'link' => [
				'url' => add_query_arg( 'job_title', $args['title'] . ' - ' . $args['location'], $args['apply_link'] ),
				'title' => 'Apply',
			],
			'type' => 'secondary',
			'wrapper' => true,
		]);
	}
	?>

</section>
