<?php
use Lean\Load;

$args = wp_parse_args( $args, [
  'title' => '',
  'boxes' => [],
  'bg_color' => 'gray',
]);

if ( empty( $args['boxes'] ) ) {
	return;
}

Load::molecule( 'navigation/link-anchor', [
	'title' => $args['title'],
	'default' => 'verticals',
]);
?>

<section class="o__verticals o__verticals--<?php echo esc_attr( $args['bg_color'] ); ?>">

	<?php
	Load::molecule( 'content/content', [
		'title' => $args['title'],
		'theme' => 'gray' === $args['bg_color'] ? 'dark' : 'light',
		'aligned' => 'centered',
	]);
	?>

  <div class="container container--no-padding container--padding-mobile">

		<div class="o__verticals-boxes">
			<?php
			foreach ( $args['boxes'] as $box ) {
				$box_args = [
					'link' => $box['link'],
					'description' => $box['description'],
					'thumbnail' => $box['image'],
					'theme' => 'gray' === $args['bg_color'] ? 'light' : 'dark',
				];
				Load::molecule( 'box/vertical-box', $box_args );
			}
			?>
		</div>

	</div>
</section>
