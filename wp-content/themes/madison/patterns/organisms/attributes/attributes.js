const breakpoints = require( 'atoms/base/breakpoints' );
const from = require( 'array.from' );

/**
 * Function that activates the Attributes slider (visible on mobile).
 */
function attributesSlider() {
  const sectionContainer = document.querySelector( '.o__attributes' );
  if ( !sectionContainer ) {
    return;
  }

  let slider = null;
  const sliderContainer = sectionContainer.querySelector( '.o__attributes-boxes' );
  const slides = from( sliderContainer.querySelectorAll( '.m__attributes-box' ) );
  const $sliderContainer = $( sliderContainer );
  const boxesSeparation = 36;
  const boxesPadding = 55;

  setSlider();

  window.addEventListener( 'resize', setSlider );
   
  /**
   * Initialize slider.
   */   
  function setSlider() {
    if ( slides.length > 0 && window.innerWidth < breakpoints.tablet ) {
      slider = $sliderContainer.owlCarousel({
        margin: boxesSeparation,
        stagePadding: boxesPadding,
        center: true,
        smartSpeed: 550,
        responsive: {
          0: {
            items: 1
          },
          425: {
            items: 2
          },
          1024: {
            items: 3
          }
        }        
      });
    } else if ( slider ) {
      $sliderContainer.data( 'owl.carousel' ).destroy();
      slider = null;
    }
  }
}

export default attributesSlider;