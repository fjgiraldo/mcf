<?php
use Lean\Load;

$args = wp_parse_args( $args, [
  'title' => '',
  'introduction' => '',
  'boxes' => [],
  'button' => [],
  'bg_color' => 'blue',
  'icon_spacing' => false,
]);

if ( empty( $args['boxes'] ) ) {
	return;
}

Load::molecule( 'navigation/link-anchor', [
	'title' => $args['title'],
	'default' => 'attributes',
]);
?>

<section class="o__attributes o__attributes--<?php echo esc_attr( $args['bg_color'] ); ?>">
  <div class="container container--no-padding">

		<?php if ( $args['title'] ) : ?>
		<h2 class="o__attributes-title <?php if ( $args['introduction'] ) : ?>o__attributes-title--with-intro<?php endif; ?> fade">
			<?php echo esc_html( $args['title'] ); ?>
		</h2>	
		<?php endif; ?>

		<?php if ( $args['introduction'] ) : ?>
		<p class="o__attributes-introduction fade">
			<?php echo esc_html( $args['introduction'] ); ?>
		</p>
		<?php endif; ?>

		<div class="o__attributes-boxes o__attributes-boxes--count-<?php echo count( $args['boxes'] ); ?> owl-carousel owl-theme">  
		<?php foreach ( $args['boxes'] as $box ) {
			Load::molecule( 'box/attributes-box', [
				'icon' => $box['icon'],
				'title' => $box['title'],
				'content' => $box['content'],
				'icon_spacing' => $args['icon_spacing'],
			]);
		}
		?>
		</div>

		<?php if ( ! empty( $args['button']['url'] ) ) : ?>
		<div class="o__attributes-link">
		<?php
		Load::atom( 'buttons/link-as-button', [
			'link' => $args['button'],
			'type' => 'blue' === $args['bg_color'] ? 'primary' : 'secondary',
		]);
		?>
		</div>
		<?php endif; ?>

	</div>
</section>
