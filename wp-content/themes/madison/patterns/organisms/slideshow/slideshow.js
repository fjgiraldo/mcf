const breakpoints = require( 'atoms/base/breakpoints' );
const from = require( 'array.from' );

/**
 * Starts the image carousel.
 */
function slideshow() {
  const sectionContainer = document.querySelector( '.o__slideshow' );

  if ( !sectionContainer ) {
    return;
  }

  const sliderContainer = sectionContainer.querySelector( '.o__slideshow-slider' );
  const mainContainer = sectionContainer.querySelector( '.container' );
  const textContainer = from( sectionContainer.querySelectorAll( '.o__slideshow-text-box-container' ) );
  const textSubContainer = sectionContainer.querySelector( '.o__slideshow-text-box-subcontainer' );
  const sliderHiddenTextContainer = from( sectionContainer.querySelectorAll( '.o__slideshow-slide-text-content' ) );
  const slides = from( sectionContainer.querySelectorAll( '.o__slideshow-slide' ) );
  const navIcon = sectionContainer.querySelector( '.o__slideshow-nav-icon' );
  const currentSlide = sectionContainer.querySelector( '.o__slideshow-text-box-slide-counter__current' );
  const showSlideButton = sectionContainer.querySelector( '.a__btn' );
  const closeSlideButton = sectionContainer.querySelector( '.o__slideshow-close' );
  const sliderBox = sectionContainer.querySelector( '.o__slideshow-text-box-slider-container' );
  let sliderNavIcons = '';
  const $slider = $( sliderContainer );
  const $sliderBoxText = $( sliderBox ).find( '.o__slideshow-text-box-container' );


  if ( navIcon ) {
    sliderNavIcons = [navIcon.innerHTML, navIcon.innerHTML];
  }

  initSlider( false );
  setLeftMargin();

  window.addEventListener( 'resize', setLeftMargin );

  showSlideButton.addEventListener( 'click', () => {
    sliderBox.classList.add( 'activate' ); 
    sliderContainer.classList.add( 'activate' ); 
    reloadSlider( true );
  });

  closeSlideButton.addEventListener( 'click', () => {
    sliderBox.classList.remove( 'activate' ); 
    sliderContainer.classList.remove( 'activate' ); 
    reloadSlider( false );
  });

  /**
   * Initiates the slider.
   *
   * @param {boolean} drag - Enable drag on slider.
   */
  function initSlider( drag ) {
    if ( slides.length > 0 ) {
      $slider.owlCarousel({
        items: 1,
        nav: true,
        smartSpeed: 550,
        navText: sliderNavIcons,
        navContainer: '.o__slideshow .carousel-custom-nav',
        dots: false,
        onChanged: onSlideChanged,
        mouseDrag: drag,
        touchDrag: drag,
      });    
    }
  }

  /**
   * Reloads the slider.
   *
   * @param {boolean} drag - Enable drag on slider.
   */
  function reloadSlider( drag ) {
    if ( $slider.length ) {
      $slider.trigger( 'destroy.owl.carousel' );
      $sliderBoxText.append( '<div class="carousel-custom-nav"></div>' );
      initSlider( drag );
    }
  }

  /**
   * Sets the text box left margin so it is aligned with the main container.
   */
  function setLeftMargin() {
    if ( mainContainer && textContainer && window.innerWidth > breakpoints.tablet ) {
      const left = mainContainer.offsetLeft;
      textContainer.forEach( ( element ) => {
        element.style.marginLeft = `${left}px`;
      });
    } else {
      textContainer.forEach( ( element ) => {
        element.style.marginLeft = '0px';
      });      
    }
  }  

  /**
   * When a slide changes it updates the description of each slide on the fixed box out of the slider. 
   * And updates the counter of the slide.
   *
   * @param {object} event - the changed data.   
   */
  function onSlideChanged( event ) {
    let index = parseInt( event.item.index ); 
    index = isNaN( index ) ? 1 : index + 1;

    const currentTextContainer = sliderHiddenTextContainer[index];
    setDescriptionText( currentTextContainer );

    if ( currentSlide ) {
      const formattedNumber = ( `0${index}` ).slice( -2 );
      currentSlide.innerHTML = formattedNumber;
    }
  }

  /**
   * Clones the text element on the fixed box out of the slider.
   *
   * @param {object} text - the text to change.   
   */
  function setDescriptionText( text ) {
    if ( textSubContainer && text ) {
      const textElements = text.cloneNode( true );
      textSubContainer.innerHTML = '';
      textSubContainer.appendChild( textElements );
    }
  }
}

module.exports = slideshow;