<?php
use Lean\Load;
use Madison\Helpers;

$args = wp_parse_args( $args, [
	'title' => '',
	'introduction' => '',
	'initial_box_title' => '',
	'initial_box_description' => '',
	'slides' => [],
	'bg_color' => 'blue',
]);

if ( empty( $args['slides'] ) ) {
	return;
}

Load::molecule( 'navigation/link-anchor', [
	'title' => $args['title'],
	'default' => 'slideshow',
]);
?>

<section class="o__slideshow o__slideshow--<?php echo esc_attr( $args['bg_color'] ); ?>">

	<div class="container o__slideshow-intro-container">
		<?php
		Load::molecule( 'content/content', [
			'title' => $args['title'],
			'content' => $args['introduction'],
			'aligned' => 'centered',
			'theme' => 'blue' === $args['bg_color'] ? 'light' : 'dark',
		]);
		?> 
	</div>

	<div class="o__slideshow-slider-container">

		<div class="o__slideshow-text-box">

			<div class="o__slideshow-text-box-initial-container">
				<div class="o__slideshow-text-box-container ">
					<div class="o__slideshow-slide-text-content">
						<h3 class="o__slideshow-text-box-title"><?php echo esc_html( $args['initial_box_title'] ); ?></h3>
						<div class="o__slideshow-text-box-description"><?php echo wp_kses_post( $args['initial_box_description'] ); ?></div>
					</div> 
					<?php
					Load::atom( 'buttons/button', [
						'label' => 'View Slideshow',
						'type' => 'secondary',
					]);
					?>
				</div>  
			</div>

			<div class="o__slideshow-text-box-slider-container">
				<div class="o__slideshow-text-box-container">
					<button class="o__slideshow-close"><?php use_icon( 'close-button-blue', 'o__slideshow-close-icon' ); ?></button>
					<div class="o__slideshow-text-box-slide-counter">  
						<span class="o__slideshow-text-box-slide-counter__current">1</span>
						/ <?php echo esc_html( sprintf( '%02d', count( $args['slides'] ) ) ); ?>
					</div>
					<div class="o__slideshow-text-box-subcontainer"></div>
					<div class="carousel-custom-nav"></div>  
				</div>  
			</div> 

		</div>

		<div class="o__slideshow-slider owl-carousel owl-theme">
			<?php foreach ( $args['slides'] as $index => $slide ) : ?>
				<div class="o__slideshow-slide">
					<div class="hidden">
						<div class="o__slideshow-slide-text-content">
							<h3 class="o__slideshow-text-box-title"><?php echo esc_html( $slide['title'] ); ?></h3>
							<div class="o__slideshow-text-box-description"><?php echo wp_kses_post( $slide['description'] ); ?></div>
						</div>        
					</div>
					<?php Helpers::bg_image( $slide['image'], '.o__slideshow-slide-image-index-' . $index ); ?>
					<div class="o__slideshow-slide-image o__slideshow-slide-image-index-<?php echo esc_attr( $index ); ?>">
					</div>
				</div>
			<?php endforeach; ?>
		</div>

		<span class="o__slideshow-nav-icon"><?php use_icon( 'side-arrow-lightblue' ); ?></span>

	</div>

</section>
