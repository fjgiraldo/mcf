const $ = require( 'jquery' );

/**
 * Initialize the listing of transactions
 */
function displayTransactions() {
  const transactionsSection = document.querySelector( '.o__transactions' );
  if ( !transactionsSection ) {
    return;
  }  

  const transactionsContainer = transactionsSection.querySelector( '.o__transactions-container' );
  const filterYear = transactionsSection.querySelector( '.o__transactions-filters-by-year' );
  const filterSector = transactionsSection.querySelector( '.o__transactions-filters-by-sector' );
  const filterIcon = transactionsSection.querySelector( '.o__transactions-filters-icon' );
  const loadMoreButton = transactionsSection.querySelector( '.a__btn' );
  const loader = transactionsSection.querySelector( '.loader' );
  const loaderActiveClass = 'loader--active';
  const containerLoadingClass = 'o__transactions--loaded';  
  const $filterSector = $( filterSector );

  let pageToLoad = 1;
  let year = null;
  let sector = null;
  let labelValue = '';

  const selectricOptions = {
    disableOnMobile: false,
    nativeOnMobile: false,
    arrowButtonMarkup: filterIcon.innerHTML,
    onOpen: ( element ) => {
      const parent = element.closest( '.selectric-wrapper' );
      const label = parent.querySelector( '.label' );
      const defaultOption = parent.querySelector( '.selectric-items li' );
      labelValue = label.innerHTML;
      label.innerHTML = defaultOption.innerHTML;
    },
    onClose: ( element ) => {
      const parent = element.closest( '.selectric-wrapper' );
      const label = parent.querySelector( '.label' );
      label.innerHTML = labelValue;
    },
    onChange: ( element ) => {
      const label = element.options[element.selectedIndex].value;
      if ( 'transactions_by_year' === element.id ) {
        year = label;
      } else {
        sector = label;
      }      
      filterTransactions( pageToLoad, year, sector );      
    }
  };

  // Load select styling.
  $( filterYear ).selectric( selectricOptions );
  $filterSector.selectric( selectricOptions );  

  // Load initial transactions.
  clearContainer();
  selectSectorFromUrl();
  fetchTransactions( pageToLoad );

  // Set Load More button event.
  loadMoreButton.addEventListener( 'click', () => fetchTransactions( pageToLoad, year, sector ) );

  /**
   * Display transactions on the page
   *
   * @param {number} page The pagination, page number
   * @param {string} year The filtered year
   * @param {string} sector The filtered sector
   */
  function fetchTransactions( page, year, sector ) {
    const apiURL = createApiUrl( page, year, sector );

    loader.classList.add( loaderActiveClass );
    transactionsSection.classList.remove( containerLoadingClass );
    loadMoreButton.classList.remove( 'active' );

    fetch( apiURL, window.madison.apiHeaders )
      .then( ( response ) => {
        if ( response.status === 200 ) {
          return response.json();
        } else {
          throw new Error( 'Error.' );
        }
      })
      .then( ( posts ) => {
        onFetchEnd();
        render( posts.data );
        loadMore( page, posts.max_pages );
      })
      .catch( () => {
        onFetchEnd();
        transactionsContainer.innerHTML = 'An error ocurred. Please try again by reloading the page.';
      });
  }

  /**
   * Dom elements configurations after the fetch finishes.
   */
  function onFetchEnd() {
    loader.classList.remove( loaderActiveClass );
    transactionsSection.classList.add( containerLoadingClass );
  }

  /**
   * Creates the API url
   *
   * @param {number} page The pagination, page number
   * @param {string} year The filtered year
   * @param {string} sector The filtered sector
   *
   * @return {string} api url
   */
  function createApiUrl( page, year, sector ) {
    window.madison = 'madison' in window ? window.madison : {};
    let apiURL = `${window.madison.api_url}transactions?page=${page}`;

    if ( year && 'all' !== year ) {
      apiURL = `${apiURL}&year=${year}`;
    }
    if ( sector && 'all' !== sector ) {
      apiURL = `${apiURL}&sector=${sector}`;
    } 

    return apiURL;
  }

  /**
   * Clears container and shows transactions.
   *
   * @param {number} page The pagination, page number
   * @param {string} year The filtered year
   * @param {string} sector The filtered sector
   */
  function filterTransactions( page, year, sector ) {
    clearContainer();
    pageToLoad = 1;
    fetchTransactions( pageToLoad, year, sector );
  }

  /**
   * Display transactions on the page
   *
   * @param {Array} posts Array of Transactions in JSON
   */
  function render( posts ) {
    if ( posts.length > 0 ) {
      transactionsContainer.insertAdjacentHTML( 'beforeend', posts.join( ' ' ) );     
    } else {
      transactionsContainer.innerHTML = 'No Transactions Found.';
    }
  }

  /**
   * Clear the container.
   */
  function clearContainer() {
    transactionsContainer.innerHTML = '';
  }

  /**
   * Toggle the Load More button and save the next page to load variable.
   *
   * @param {number} actualPage The actual page
   * @param {number} totalPages The total pages of Transactions
   */
  function loadMore( actualPage, totalPages ) {
    if ( actualPage < totalPages ) {
      loadMoreButton.classList.add( 'active' );
      const nextPage = pageToLoad + 1;
      if ( nextPage <= totalPages ) {
        pageToLoad = nextPage;
      }
    } else {
      loadMoreButton.classList.remove( 'active' );
    }
  }

  /**
   * Loads the transactions from the sector given as hashtag in the url.
   */
  function selectSectorFromUrl() {
    if ( window.location.hash ) {
      const hash = window.location.hash.substring( 1 );
      selectOptionFromValue( filterSector.options, hash );
      $filterSector.selectric( 'refresh' );
    }
  }

  /**
   * Sets the option from a select input given a value.
   *
   * @param {element} options The sectors select element options
   * @param {string} value The value of the option to select
   */
  function selectOptionFromValue( options, value ) {
    for ( var i = 0; i < options.length; i++ ) {
      if ( options[i].value === value ) {
        options[i].selected = true;
        break;
      }
    }    
  }
}

module.exports = displayTransactions;