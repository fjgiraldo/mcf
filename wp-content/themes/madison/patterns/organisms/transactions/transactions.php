<?php
use Lean\Load;
use Madison\Modules\Transactions\Transactions as Transaction;

$args = wp_parse_args( $args, [
	'title' => '',
	'introduction' => '',
]);

Load::molecule( 'navigation/link-anchor', [
	'title' => $args['title'],
	'default' => 'transactions',
]);
?>

<section class="o__transactions">
	<div class="container container--no-padding">

		<?php
		Load::molecule( 'content/content', [
			'title' => $args['title'],
			'content' => $args['introduction'],
			'theme' => 'dark',
		]);
		?>
		<div class="o__transactions-filters">

			<span class="o__transactions-filters-icon"><?php use_icon( 'side-arrow-lightblue' ); ?></span>

			<select class="o__transactions-filters-by-year" name="transactions_by_year" id="transactions_by_year">
				<option value="none">Filter by year</option>
				<option value="all">All</option>
				<?php
				$years = Transaction::get_all_years();
				foreach ( $years as $year ) :
				?>
				<option value="<?php echo esc_attr( $year['year'] ); ?>"><?php echo esc_html( $year['year'] ); ?></option>
				<?php endforeach; ?>
			</select>
			
			<select class="o__transactions-filters-by-sector" name="transactions_by_sector" id="transactions_by_sector">
				<option value="none">Filter by sector</option>
				<option value="all">All</option>
				<?php
				$verticals = Transaction::get_all_verticals();
				foreach ( $verticals as $vertical ) :
				?>				
				<option value="<?php echo esc_attr( $vertical->slug ); ?>"><?php echo esc_html( $vertical->name ); ?></option>
				<?php endforeach; ?>			
			</select>
		</div>

		<div class="o__transactions-container">
		</div>  

		<div class="loader">
			<div class="loader-animation"></div>
		</div>		

		<?php
		Load::atom( 'buttons/button', [
			'label' => 'Load More',
			'type' => 'secondary',
		]);
		?>

	</div>
</section>
