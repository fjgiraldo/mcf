<?php
use Lean\Load;

$args = wp_parse_args( $args, [
  'title' => '',
  'content' => '',
  'link' => [],
  'bg_color' => 'blue',
]);

Load::molecule( 'navigation/link-anchor', [
  'title' => $args['title'],
  'default' => 'copy-block',
]);
?>

<section class="o__copy-block-text o__copy-block-text--<?php echo esc_attr( $args['bg_color'] ); ?>">
  <div class="container container--no-padding container--padding-mobile">

		<div class="o__copy-block-text-subcontainer">
			<?php
			Load::molecule( 'content/content', [
				'title' => $args['title'],
				'content' => $args['content'],
				'theme' => 'blue' === $args['bg_color'] ? 'light' : 'dark',
			]);

			Load::atom( 'buttons/link-as-button', [
				'link' => $args['link'],
				'type' => 'blue' === $args['bg_color'] ? 'primary' : 'secondary',
			]);
			?>
		</div>

	</div>
</section>
