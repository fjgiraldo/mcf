<?php
use Lean\Load;

$args = wp_parse_args( $args, [
  'title' => '',
  'content' => '',
  'link' => [],
  'media_type' => 'none',
  'media_image' => '',
  'media_video' => '',
  'media_video_brightcove' => '',
  'media_video_image' => '',
  'bg_color' => 'blue',
]);

Load::molecule( 'navigation/link-anchor', [
  'title' => $args['title'],
  'default' => 'copy-block-media',
]);
?>

<section class="o__copy-block-media o__copy-block-media--<?php echo esc_attr( $args['bg_color'] ); ?>">
  <div class="container container--no-padding container--padding-mobile">

		<div class="o__copy-block-media-text">
			<?php
			$theme = 'blue' === $args['bg_color'] ? 'light' : 'dark';

			Load::molecule( 'content/content', [
				'title' => $args['title'],
				'content' => $args['content'],
				'theme' => $theme,
			]);

			Load::molecule( 'link/link', [
				'link' => $args['link'],
				'theme' => $theme,
			]);
			?>
		</div>

		<?php if ( 'none' !== $args['media_type'] ) : ?>
		<div class="o__copy-block-media-media">
			<?php
			Load::atoms( 'media/media', [
				'type' => $args['media_type'],
				'image' => $args['media_image'],
				'video' => $args['media_video'],
				'video_brightcove' => $args['media_video_brightcove'],
				'video_image' => $args['media_video_image'],
			]);
			?>      
		</div>
		<?php endif; ?>

	</div>
</section>
