<?php
use Lean\Load;

$args = wp_parse_args( $args, [
  'title' => '',
  'verticals' => [],
  'bg_color' => 'white',
]);

if ( empty( $args['verticals'] ) ) {
  return;
}

Load::molecule( 'navigation/link-anchor', [
	'title' => $args['title'],
	'default' => 'verticals-teaser',
]);
?>

<section class="o__verticals-teaser o__verticals-teaser--<?php echo esc_attr( $args['bg_color'] ); ?>">
	<div class="container container-no-padding container--padding-mobile">

		<?php if ( $args['title'] ) : ?>
		<h2 class="o__verticals-teaser-title"><?php echo esc_html( $args['title'] ); ?></h2>
		<?php endif; ?>
		<div class="o__verticals-teaser-links">
			<?php foreach ( $args['verticals'] as $vertical ) : ?>
				<div class="o__verticals-teaser-link-item">
				<?php
				Load::molecule( 'link/link', [
					'link' => $vertical['link'],
					'theme' => 'dark',
				]);
				?>
				</div>
			<?php endforeach; ?>
		</div>

	</div>
</section>
