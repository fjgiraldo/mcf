const breakpoints = require( 'atoms/base/breakpoints' );
const ScrollMagic = require( 'scrollmagic' );

const fixedClass = 'o__header--shrink';
const header = document.querySelector( '.o__header' );

/**
 * Function that is used to fix the header when the page is scrolled.
 * Funcitionnality for desktop view only. Supports device rotation.
 */
function fixHeaderOnScroll() {
  if ( !header ) {
    return;
  }

  addClass();
  window.addEventListener( 'resize', addClass );
}

/**
 * Function that adds the Fixed class to the header on desktop view.
 */
function addClass() {
  let offset = 40;

  if ( window.innerWidth > breakpoints.tablet ) {
    offset = 80;
  } 

  new ScrollMagic.Scene({ offset: offset })
    .setClassToggle( header, fixedClass )
    .addTo( window.madison.controller ); 
}

export default fixHeaderOnScroll;
