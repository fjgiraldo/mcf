<?php
use Lean\Load;

$logo = get_field( 'general_options_logo', 'options' );
?>

<header
	class="o__header"
	role="banner"
	itemscope
	itemtype="http://schema.org/WPHeader">

  <div class="container">

		<div class="logo">
			<?php if ( $logo ) : ?>
			<a class="logo-link" itemprop="mainEntityOfPage" href="<?php bloginfo( 'url' ); ?>" title="<?php bloginfo( 'name' ); ?>" >
				<img src="<?php echo esc_url( $logo ); ?>" alt="<?php bloginfo( 'name' ); ?>" />
			</a>
			<?php endif; ?>
		</div>

		<?php Load::molecule( 'navigation/primary-nav' ); ?>

		<?php Load::molecule( 'search/search-icon' ); ?>

		<?php Load::atom( 'hamburger/hamburger' ); ?>

  </div>

</header>

<?php Load::molecule( 'search/search-input' ); ?>
