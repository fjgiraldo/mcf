<?php
use Lean\Load;
use Madison\Modules\TeamMembers\TeamMembers;

$args = wp_parse_args( $args, [
	'title' => '',
	'introduction' => '',
	'category_chosen' => TeamMembers::TAXONOMY, // department or vertical.
	'members_by_department' => [],
	'members_by_vertical' => [],
	'bg_color' => 'gray',
]);

if ( empty( $args['members_by_department'] ) && empty( $args['members_by_vertical'] ) ) {
	return;
}

if ( TeamMembers::TAXONOMY_VERTICAL === $args['category_chosen'] && ! $args['members_by_vertical'] instanceof WP_Term ) {
	return;
}

Load::molecule( 'navigation/link-anchor', [
	'title' => $args['title'],
	'default' => 'team',
]);
?>

<section class="o__team o__team--<?php echo esc_attr( $args['bg_color'] ); ?>">
	<div class="container container--no-padding container--padding-mobile">

		<?php
		$theme = 'blue' === $args['bg_color'] ? 'light' : 'dark';

		Load::molecule( 'content/content', [
			'title' => $args['title'],
			'content' => $args['introduction'],
			'theme' => $theme,
		]);
		?> 

	</div>

	<div class="container container--no-padding-tablet">

		<?php
		if ( TeamMembers::TAXONOMY === $args['category_chosen'] ) {
			Load::molecule( 'tabs/team-tabs', [
				'members_by_category' => $args['members_by_department'],
			]);
		}
		?>

		<div class="o__team-boxes">  

			<?php if ( 'vertical' === $args['category_chosen'] ) : ?>

			<div class="o__team-boxes-category">
				<?php
				$members = TeamMembers::get_members_by_term([
					'query' => [
						'tax_query' => [
							[
								'taxonomy' => TeamMembers::TAXONOMY_VERTICAL,
								'terms' => $args['members_by_vertical']->term_id,
							],
						],
					],
				]);
				foreach ( $members['data'] as $member ) {
					Load::molecule( 'box/team-box', $member );
				}
				?>
			</div>

			<?php else : ?>

			<div class="o__team-boxes-category"></div>

			<div class="loader">
				<div class="loader-animation"></div>
			</div>

			<div class="container o__team-button-container">
			<?php
			Load::atom( 'buttons/button', [
				'label' => 'View More',
				'type' => 'secondary',
				'class' => 'o__team-view-more-button',
			]);
			?>
			</div>

			<?php endif; ?>

			<?php Load::molecule( 'modal/team-modal' ); ?>

		</div>

	</div>
</section>
