<?php
use Lean\Load;

$args = wp_parse_args( $args, [
	'capabilities' => [],
	'bg_color' => 'white',
]);

if ( empty( $args['capabilities'] ) ) {
	return;
}

Load::molecule( 'navigation/link-anchor', [
	'title' => '',
	'default' => 'capabilities-teaser',
]);
?>

<section class="o__capabilities-teaser o__capabilities-teaser--<?php echo esc_attr( $args['bg_color'] ); ?>">
	<div class="container container--no-padding container--padding-mobile">
		<?php foreach ( $args['capabilities'] as $capability ) : ?>
		<div class="o__capabilities-teaser-item">
			<div class="o__capabilities-teaser-item-link">
				<div class="o__capabilities-teaser-item-link-subcontainer">
				<?php
				Load::molecule( 'link/link', [
					'link' => $capability['link'],
					'theme' => 'blue' === $args['bg_color'] ? 'light' : 'dark',
				]);
				?>
				</div>
			</div>
			<div class="o__capabilities-teaser-item-row-bottom">
				<p class="o__capabilities-teaser-item-content fade"><?php echo esc_html( $capability['content'] ); ?></p>
				<div class="o__capabilities-teaser-item-line"></div>
			</div>
		</div>
		<?php endforeach; ?>
	</div>
</section>
