<?php
use Lean\Load;

$args = wp_parse_args( $args, [
	'title' => '',
	'text' => '',
	'image' => '',
	'links' => '',
	'box_color' => 'blue',
	'box_alignment' => 'left',
	'bg_color' => 'gray',
]);

if ( empty( $args['links'] ) ) {
	return;
}

Load::molecule( 'navigation/link-anchor', [
	'title' => $args['title'],
	'default' => 'menu',
]);
?>

<section class="o__menu o__menu--<?php echo esc_attr( $args['bg_color'] ); ?> o__menu--box-<?php echo esc_attr( $args['box_alignment'] ); ?>">
	<div class="container container--no-padding">
		<?php if ( $args['title'] ) : ?>
		<h2 class="o__menu-title fade">
			<?php echo esc_html( $args['title'] ); ?>
		</h2>	
		<?php endif; ?>
		<div class="o__menu-subcontainer">

			<?php
			$image_args = [
				'alt' => $args['title'],
				'class' => 'o__menu-image',
			];
			echo wp_get_attachment_image( $args['image'], 'full', false, $image_args );
			?>

			<div class="o__menu-box o__menu-box--<?php echo esc_attr( $args['box_color'] ); ?>">
				<div class="o__menu-box-subcontainer <?php if ( empty( $args['text'] ) ) : ?>o__menu-box-subcontainer--no-text<?php endif; ?>">
					<div class="o__menu-box-subcontainer-content fade">
						<?php echo esc_html( $args['text'] ); ?>
					</div>
					<div class="o__menu-links">
					<?php
					foreach ( $args['links'] as $link ) {
						Load::molecule( 'link/link', [
							'link' => $link['link'],
							'theme' => 'light',
						]);
					}
					?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
