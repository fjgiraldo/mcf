<?php
use Lean\Load;

$args = wp_parse_args( $args, [
  'title' => '',
  'text' => '',
  'thumbnail' => '',
  'category' => '',
  'type' => 'text', // [ text, link, file ].
  'link_url' => '',
  'theme' => 'light', // [ light, dark ].
  'phantom' => false,
]);
?>

<div class="m__media-insight-box m__media-insight-box--<?php echo esc_attr( $args['theme'] ); ?> <?php if ( ! $args['thumbnail'] ) : ?>m__media-insight-box--no-thumbnail<?php endif; ?> <?php if ( $args['phantom'] ) : ?>phantom<?php endif; ?>">
	
	<div class="m__media-insight-box-content">

		<?php if ( $args['category'] ) : ?>
		<span class="m__media-insight-box-content-category"><?php echo esc_html( $args['category'] ); ?></span>
		<?php endif; ?>

		<div class="m__media-insight-box-content-subcontainer">

			<?php if ( $args['title'] ) : ?>
			<h3 class="m__media-insight-box-content-title">
				<a
				<?php if ( 'text' !== $args['type'] ) : ?>target="_blank" rel="noreferrer noopener"<?php endif; ?> 
				href="<?php echo esc_url( $args['link_url'] ); ?>">
					<?php echo esc_html( $args['title'] ); ?>
				</a>
			</h3>
			<?php endif; ?>

			<?php if ( $args['text'] ) : ?>
			<p class="m__media-insight-box-content-text"><?php echo esc_html( $args['text'] ); ?></p>
			<?php endif; ?>

		</div>

		<?php if ( $args['link_url'] ) : ?>
		<a 
		class="m__media-insight-box-content-link"
		<?php if ( 'text' !== $args['type'] ) : ?>target="_blank" rel="noreferrer noopener"<?php endif; ?> 
		href="<?php echo esc_url( $args['link_url'] ); ?>">
			<?php
			$atom = 'file' === $args['type'] ? 'icons/file-icon' : 'icons/circular-arrow-icon';
			Load::atom( $atom, [
				'color' => 'light' === $args['theme'] ? 'red' : 'white',
			]);
			?>
		</a>
	<?php endif; ?>

  </div>

  <?php if ( $args['thumbnail'] ) : ?>
  <img src="<?php echo esc_url( $args['thumbnail'] ); ?>" alt="<?php echo esc_attr( $args['title'] ); ?>" class="m__media-insight-box-thumbnail">
  <?php endif; ?>

</div>
