<?php
use Lean\Load;
use Madison\Helpers;

$args = wp_parse_args( $args, [
	'number' => '',
	'prefix' => '',
	'suffix' => '',
	'content' => '',
]);
?>

<div class="m__facts-figures-box">
	<?php if ( $args['number'] ) : ?>
	<div class="m__facts-figures-box-head">
		<div class="m__facts-figures-box-data fade-only">
			<span class="m__facts-figures-box-prefix"><?php echo esc_html( $args['prefix'] ); ?></span>
			<span 
				class="m__facts-figures-box-number"
				data-decimals="<?php echo esc_attr( Helpers::count_decimals( $args['number'] ) ); ?>"
				data-endnumber="<?php echo esc_attr( $args['number'] ); ?>">
				0
			</span>
			<span class="m__facts-figures-box-suffix"><?php echo esc_html( $args['suffix'] ); ?></span>
		</div>
	</div>
	<?php endif; ?>
	<h3 class="m__facts-figures-box-content"><?php echo esc_html( $args['content'] ); ?></h3>
</div>
