<?php
use Lean\Load;

$args = wp_parse_args( $args, [
	'icon' => '',
	'title' => '',
	'content' => '',
	'icon_spacing' => false,
]);
?>

<div class="m__attributes-box <?php if ( $args['icon_spacing'] ) : ?>m__attributes-box--more-space<?php endif; ?> <?php if ( $args['icon'] ) : ?>m__attributes-box--with-icon<?php endif; ?>">
	<div class="m__attributes-box-head">
		<?php if ( $args['icon'] ) : ?>
		<img src="<?php echo esc_url( $args['icon'] ); ?>" class="m__attributes-box-icon fade" alt="<?php echo esc_attr( $args['title'] ); ?>">
		<?php endif; ?>
		<?php if ( $args['title'] ) : ?>
		<h3 class="m__attributes-box-title fade"><?php echo esc_html( $args['title'] ); ?></h3>
		<?php endif; ?>
	</div>
	<?php if ( $args['content'] ) : ?>
	<p class="m__attributes-box-content fade"><?php echo wp_kses_post( $args['content'] ); ?></p>
	<?php endif; ?>
</div>
