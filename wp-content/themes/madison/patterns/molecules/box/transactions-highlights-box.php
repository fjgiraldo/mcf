<?php
use Lean\Load;

$args = wp_parse_args( $args, [
	'logo_top' => '',
	'quantity' => '',
	'text' => '',
	'transactions_type' => '',
	'logo_bottom' => '',
	'date' => '',
]);
?>

<div class="m__transactions-highlights-box">

	<div class="m__transactions-highlights-box-head">
		<?php if ( $args['logo_top'] ) : ?>
		<div class="m__transactions-highlights-box-logo-top-container">
			<img class="m__transactions-highlights-box-logo-top" src="<?php echo esc_url( $args['logo_top'] ); ?>" alt="Transaction Logo">
		</div>
		<?php endif; ?>
		<?php if ( $args['quantity'] ) : ?>
		<div class="m__transactions-highlights-box-quantity"><?php echo esc_html( $args['quantity'] ); ?></div>
		<?php endif; ?>
		<div class="m__transactions-highlights-box-separator"></div>
		<?php if ( $args['text'] ) : ?>
		<div class="m__transactions-highlights-box-text"><?php echo esc_html( $args['text'] ); ?></div>
		<?php endif; ?>
		<?php if ( $args['transactions_type'] ) : ?>
		<p class="m__transactions-highlights-box-type"><?php echo esc_html( $args['transactions_type'] ); ?></p>
		<?php endif; ?>		
	</div>

	<div class="m__transactions-highlights-box-foot">
		<?php if ( $args['logo_bottom'] ) : ?>
		<div class="m__transactions-highlights-box-logo-bottom-container">  
			<img class="m__transactions-highlights-box-logo-bottom" src="<?php echo esc_url( $args['logo_bottom'] ); ?>" alt="Transaction Logo">
		</div>
		<?php endif; ?>
		<?php if ( $args['date'] ) : ?>
		<div class="m__transactions-highlights-box-date"><?php echo esc_html( $args['date'] ); ?></div>
		<?php endif; ?>
	</div>

</div>
