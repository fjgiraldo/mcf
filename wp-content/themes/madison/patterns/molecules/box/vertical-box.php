<?php
use Lean\Load;

$args = wp_parse_args( $args, [
	'link' => '',
	'description' => '',
	'thumbnail' => '',
	'theme' => 'light', // [ light, dark ].
]);
?>

<div class="m__vertical-box m__vertical-box--<?php echo esc_attr( $args['theme'] ); ?>">
	
	<?php if ( $args['thumbnail'] ) : ?>
	<img src="<?php echo esc_url( $args['thumbnail'] ); ?>" alt="<?php echo esc_attr( $args['link']['title'] ); ?>" class="m__vertical-box-thumbnail">
	<?php endif; ?>

	<div class="m__vertical-box-content">

		<div class="m__vertical-box-content-subcontainer">

		<?php
		Load::molecule( 'link/link', [
			'link' => $args['link'],
			'theme' => 'dark',
		]);
		?>

		<p class="m__vertical-box-content-description fade"><?php echo esc_html( $args['description'] ); ?></p>

		</div>

	</div>

</div>
