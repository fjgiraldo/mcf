<?php
use Lean\Load;

$args = wp_parse_args( $args, [
	'logo' => '',
	'year' => '',
	'copy' => '',
]);
?>

<div class="m__award-box">

	<div class="m__award-box-head">
		<?php if ( $args['logo'] ) : ?>
		<div class="m__award-box-logo-container">
			<img class="m__award-box-logo" src="<?php echo esc_url( $args['logo'] ); ?>">
		</div>
		<?php endif; ?>
		<?php if ( $args['year'] ) : ?>
		<div class="m__award-box-year"><?php echo esc_html( $args['year'] ); ?></div>
		<?php endif; ?>
		<div class="m__award-box-separator"></div>
		<?php if ( $args['copy'] ) : ?>
		<div class="m__award-box-copy"><?php echo wp_kses_post( $args['copy'] ); ?></div>
		<?php endif; ?>
	</div>

</div>
