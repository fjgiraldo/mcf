<?php
use Lean\Load;

$args = wp_parse_args( $args, [
	'name' => '',
	'bw_picture' => '',
	'color_picture' => '',
	'title' => '',
	'bio' => '',
	'department' => '',
	'email' => '',
	'linkedin' => '',
]);
?>

<div class="m__team-box">
	<div class="m__team-box-head">
		<img class="m__team-box-bw_picture" src="<?php echo esc_url( $args['bw_picture'] ); ?>" alt="<?php echo esc_attr( $args['name'] ); ?>">
		<img class="m__team-box-color_picture" src="<?php echo esc_url( $args['color_picture'] ); ?>" alt="<?php echo esc_attr( $args['name'] ); ?>">
		<?php if ( $args['name'] ) : ?>
		<h3 class="m__team-box-name"><?php echo esc_html( $args['name'] ); ?></h3>
		<?php endif; ?>
		<?php if ( $args['title'] ) : ?>
		<h4 class="m__team-box-title"><?php echo esc_html( $args['title'] ); ?></h4>
		<?php endif; ?>
		<?php if ( $args['department'] ) : ?>
		<h5 class="m__team-box-department"><?php echo wp_kses_post( $args['department'] ); ?></h5>
		<?php endif; ?>
	</div>
	<div class="m__team-box-footer">
		<?php if ( $args['email'] ) : ?>
		<a class="m__team-box-email" href="mailto:<?php echo esc_attr( $args['email'] ); ?>"><?php use_icon( 'email-icon-navy', 'm__team-box-email-icon' ); ?></a>
		<?php endif; ?>
		<?php if ( $args['linkedin'] ) : ?>
		<a class="m__team-box-linkedin" href="<?php echo esc_url( $args['linkedin'] ); ?>" target="_blank"><?php use_icon( 'linkedin-circle', 'm__team-box-linkedin-icon' ); ?></a>
		<?php endif; ?>
		<div class="m__team-box-bio"><?php echo wp_kses_post( $args['bio'] ); ?></div>
	</div>
</div>
