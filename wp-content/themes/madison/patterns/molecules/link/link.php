<?php
use Lean\Load;

$args = wp_parse_args( $args, [
  'link' => [],
  'theme' => 'light',
]);

if ( empty( $args['link'] ) || empty( $args['link']['url'] ) ) {
  return;
}
?>

<div class="m__link m__link--<?php echo esc_attr( $args['theme'] ); ?>">
  <a 
  class="m__link-link fade"
  <?php if ( $args['link']['target'] ) : ?>target="_blank" rel="noreferrer noopener"<?php endif; ?>
  href="<?php echo esc_attr( $args['link']['url'] ); ?>">
		<span class="m__link-title"><?php echo wp_kses_post( $args['link']['title'] ); ?></span>
		<?php
		Load::atom( 'icons/circular-arrow-icon', [
			'color' => 'dark' === $args['theme'] ? 'red' : 'white',
		]);
		?>
  </a>
</div>
