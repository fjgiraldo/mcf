const from = require( 'array.from' );

/**
 * Function that activates the hover style on the icon when the text link is hovered.
 */
function hoverIcon() {
  let iconLink;
  const hoverClass = 'a__circular-arrow-icon-hover';
  const linkContainers = from( document.querySelectorAll( '.m__link' ) );
  
  linkContainers.forEach( ( linkContainer ) => {
    const options = { passive: true };
    linkContainer.addEventListener( 'mouseenter', activateIconHover, options );
    linkContainer.addEventListener( 'mouseleave', deactivateIconHover, options );
  });
 
  /**
   * Function that is called when the mouse enters the link section.
   *
   * @param {event} event - The event being fired.
   */
  function activateIconHover( event ) {
    if ( event.target ) {
      iconLink = event.target.querySelector( '.a__circular-arrow-icon' );
      if ( iconLink ) {
        iconLink.classList.add( hoverClass );
      }
    }
  }

  /**
   * Function used to restart the menu to the initial state.
   */
  function deactivateIconHover() {
    if ( iconLink ) {
      iconLink.classList.remove( hoverClass );
    }
  }
}

export default hoverIcon;