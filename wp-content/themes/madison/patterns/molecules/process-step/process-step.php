<?php
use Lean\Load;

$args = wp_parse_args( $args, [
	'index' => 1,
	'icon' => '',
	'title' => '',
	'description' => '',
	'bg_color' => 'white',
]);

if ( empty( $args['title'] ) || empty( $args['description'] ) ) {
	return;
}
?>

<div class="m__process-step m__process-step--<?php echo esc_attr( $args['bg_color'] ); ?>" data-hash="step<?php echo esc_attr( $args['index'] ); ?>">
	<div class="m__process-step-header">
		<?php if ( $args['icon'] ) : ?>
		<div class="m__process-step-icon-wrapper">
			<img class="m__process-step-icon" src="<?php echo esc_url( $args['icon'] ); ?>" alt="<?php echo esc_attr( $args['title'] ); ?>">
		</div>
		<?php endif; ?>
		<h3 class="m__process-step-title"><?php echo esc_html( $args['title'] ); ?></h3>
	</div>
	<p class="m__process-step-description"><?php echo wp_kses_post( $args['description'] ); ?></p>
</div>
