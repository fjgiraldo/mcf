<?php
use Lean\Load;

$title = get_the_title() ? rawurlencode( get_the_title() ) : '';
$excerpt = get_the_excerpt() ? rawurlencode( get_the_excerpt() ) : '';
?>

<div class="m__share-buttons">

	<div class="m__share-buttons-label">Share</div>

	<?php
	$social_networks = [
		'facebook' => sprintf( 'https://www.facebook.com/sharer/sharer.php?u=%s', esc_url( get_permalink() ) ),
		'twitter' => sprintf( 'http://twitter.com/share?text=%s&url=%s', esc_attr( $title ),  esc_url( get_permalink() ) ),
		'linkedin' => sprintf(
			'http://www.linkedin.com/shareArticle?mini=true&url=%s&title=%s&summary=%s&source=%s',
			esc_url( get_permalink() ),
			esc_attr( $title ),
			esc_attr( $excerpt ),
			esc_attr( get_site_url() )
		),
	];

	foreach ( $social_networks as $name => $url ) :
	?>

	<button
		class="m__share-buttons-link m__share-buttons-link--<?php echo esc_attr( $name ); ?>"
		data-link="<?php echo esc_attr( $url ); ?>">
		<?php
		Load::atom( 'icons/share-icon', [
				'social-media' => $name,
		]);
		?>
	</button>
	
	<?php endforeach; ?>

</div>
