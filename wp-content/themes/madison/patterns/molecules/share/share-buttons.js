const from = require( 'array.from' );

/**
 * Opens a window dialog for sharing the link.
 */
function shareLink() {
  const section = document.querySelector( '.m__share-buttons' );
  if ( !section ) {
    return;
  }

  const links = from( section.querySelectorAll( '.m__share-buttons-link' ) );
  if ( links.length === 0 ) {
    return;
  }

  links.forEach( ( link ) => {
    link.addEventListener( 'click', () => {
      window.open( link.dataset.link, 'share-dialog', 'width=626, height=436' );  
    });
  });
}

export default shareLink;