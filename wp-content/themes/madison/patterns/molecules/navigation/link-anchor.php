<?php
use Madison\Helpers;

$args = wp_parse_args( $args, [
	'title' => '',
	'default' => '',
]);

if ( ! $args['title'] && ! $args['default'] ) {
	return;
}
?>

<div 
	id="<?php echo esc_attr( Helpers::create_id_from( $args['title'], $args['default'] ) ); ?>"
	class="m__link-anchor">
</div>
