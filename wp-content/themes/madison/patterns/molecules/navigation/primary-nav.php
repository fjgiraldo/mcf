<nav class="m__primary-nav">
  <?php
  wp_nav_menu([
		'container' => 'nav',
		'container_class' => 'm__primary-nav-container',
		'theme_location' => 'main_menu',
  ]);
  ?>
</nav>
