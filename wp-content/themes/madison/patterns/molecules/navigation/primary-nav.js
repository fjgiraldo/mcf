const breakpoints = require( 'atoms/base/breakpoints' );
const from = require( 'array.from' );

/**
 * Function that is used to search the primary navigation on the header and
 * search for the items with childs to attach the mouse events.
 */
function mainNavigation() {
  const header = document.querySelector( '.o__header' );
  if ( !header ) {
    return;
  }

  const menuItemsWithSubMenu = from( header.querySelectorAll( '.menu-item-has-children' ) );
  if ( menuItemsWithSubMenu.lenght === 0 ) {
    return;
  }

  const options = { passive: true };

  displaySubmenus();

  window.addEventListener( 'resize', displaySubmenus );

  /**
   * Display submenu only on desktop.
   */
  function displaySubmenus() {
    if ( window.innerWidth > breakpoints.desktop ) {
      menuItemsWithSubMenu.forEach( onMenuItemHover );
    } else {
      menuItemsWithSubMenu.forEach( removeEvents );
    }
  }

  /**
   * Adds events listeners.
   *
   * @param {node} menuItemWithSubMenu - The menu item that has submenu.
   */
  function onMenuItemHover( menuItemWithSubMenu ) {
    menuItemWithSubMenu.addEventListener( 'mouseenter', activeSubMenu, options );
    menuItemWithSubMenu.addEventListener( 'mouseleave', disableSubMenu, options );
  }

  /**
   * Removes events listeners.
   *
   * @param {node} menuItemWithSubMenu - The menu item that has submenu.
   */
  function removeEvents( menuItemWithSubMenu ) {
    menuItemWithSubMenu.removeEventListener( 'mouseenter', activeSubMenu, options );
    menuItemWithSubMenu.removeEventListener( 'mouseleave', disableSubMenu, options );
  }

  /**
   * Activates submenu.
   *
   * @param {event} event - The event when the menu item that has submenu is being hovered.
   */
  function activeSubMenu( event ) {
    if ( event.target ) {
      const subMenu = event.target.querySelector( '.sub-menu' );

      if ( subMenu ) {
        subMenu.style.display = 'block';
      }
    }
  }

  /**
   * Deactivates submenu.
   *
   * @param {event} event - The event when the menu item that has submenu and the submenu leave the hovered state.
   */
  function disableSubMenu( event ) {
    if ( event.target ) {
      const subMenu = event.target.querySelector( '.sub-menu' );

      if ( subMenu ) {
        subMenu.style.display = '';
      }
    }
  }
}

export default mainNavigation;
