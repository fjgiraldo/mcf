<nav class="m__footer-nav">
<?php
wp_nav_menu([
	'container' => 'nav',
	'container_class' => 'm__footer-nav-container',
	'theme_location' => 'footer_menu',
]);
?>
</nav>
