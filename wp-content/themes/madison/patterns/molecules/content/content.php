<?php
$args = wp_parse_args( $args, [
  'title' => '',
  'content' => '',
  'theme' => 'light',
  'aligned' => 'left',
]);
?>

<div class="m__content m__content--<?php echo esc_attr( $args['theme'] ); ?> m__content--<?php echo esc_attr( $args['aligned'] ); ?>">
  <?php if ( ! empty( $args['title'] ) ) : ?>
  <h2 class="m__content-title fade"><?php echo esc_html( $args['title'] ); ?></h2>
  <?php endif; ?>

  <?php if ( ! empty( $args['content'] ) ) : ?>
  <div class="m__content-content fade"><?php echo wp_kses_post( $args['content'] ); ?></div>
  <?php endif; ?>
</div>
