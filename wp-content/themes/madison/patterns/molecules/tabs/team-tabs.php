<?php
use Lean\Load;

$args = wp_parse_args( $args, [
  'members_by_category' => [],
]);

if ( empty( $args['members_by_category'] ) ) {
	return;
}
?>

<?php if ( count( $args['members_by_category'] ) > 1 ) : ?>
<div class="m__team-tabs">  
	<?php foreach ( $args['members_by_category'] as $index => $category ) : ?>
	<a 
		class="m__team-tabs-category <?php if ( 0 === $index ) : ?>active<?php endif; ?>" 
		data-tab-id="<?php echo esc_attr( $category->term_id ); ?>">
		<?php echo esc_html( $category->name ); ?>
	</a>
	<?php endforeach; ?>
	<a class="m__team-tabs-category" data-tab-index="all">All</a>
</div>

<div class="m__team-tabs-mobile">  
	<select class="m__team-tabs-mobile-select" name="members_by_category_mobile" id="members_by_category_mobile">
		<?php foreach ( $args['members_by_category'] as $category ) : ?>
		<option data-tab-id="<?php echo esc_attr( $category->term_id ); ?>"><?php echo esc_html( $category->name ); ?></option>
		<?php endforeach; ?>
		<option data-tab-id="all">All</option>
	</select>
	<span class="m__team-tabs-mobile-select-icon"><?php use_icon( 'side-arrow-lightblue' ); ?></span>
</div>
<?php endif; ?>
