const $ = require( 'jquery' );
const from = require( 'array.from' );

/**
 * Tabs functionnality in Teams module.
 */
function teamTabs() {
  const section = document.querySelector( '.o__team' );
  const tabsContainer = document.querySelector( '.m__team-tabs' );
  if ( !section || !tabsContainer ) {
    return;
  }

  const tabs = from( tabsContainer.querySelectorAll( '.m__team-tabs-category' ) );
  if ( tabs.length === 0 ) {
    return;
  }

  const mobileTab = document.getElementById( 'members_by_category_mobile' );
  const selectIcon = section.querySelector( '.m__team-tabs-mobile-select-icon' );
  const postsContainer = section.querySelector( '.o__team-boxes-category' );
  const loadMoreButton = section.querySelector( '.a__btn' );
  const loader = section.querySelector( '.loader' );
  const loadMoreButtonActiveClass = 'a__btn--activate';
  const loaderActiveClass = 'loader--active';
  const activeTabClass = 'active';
  const noTextClass = 'no-text';
  let pageToLoad = 1;
  let department = null;  
  let selectArrow = '';

  if ( selectIcon ) {
    selectArrow = selectIcon.innerHTML;
  }

  const selectricOptions = {
    disableOnMobile: false,
    nativeOnMobile: false,
    arrowButtonMarkup: selectArrow,
    onChange: ( element ) => {
      const optionSelected = element.options[element.selectedIndex];
      filterPosts( optionSelected.dataset.tabId );
    }
  };

  $( mobileTab ).selectric( selectricOptions ); 

  // Load initial posts.
  clearContainer( fetchPosts, pageToLoad, tabs[0].dataset.tabId );

  // Fetch posts on tab click.
  tabs.forEach( ( tab ) => {
    tab.addEventListener( 'click', () => {
      department = tab.dataset.tabId;
      setActiveTab( tab );
      filterPosts( department );
    });    
  });

  // Set Load More button event.
  loadMoreButton.addEventListener( 'click', () => {
    fetchPosts( pageToLoad, department );
  });

  /**
   * Display posts on the page
   *
   * @param {number} page The pagination, page number
   * @param {string} department The selected department
   */
  function fetchPosts( page, department ) {
    const apiURL = createApiUrl( page, department );

    loader.classList.add( loaderActiveClass );
    loadMoreButton.classList.remove( loadMoreButtonActiveClass );

    fetch( apiURL, window.madison.apiHeaders )
      .then( ( response ) => {
        if ( response.status === 200 ) {
          return response.json();
        } else {
          throw new Error( 'Error.' );
        }
      })
      .then( ( posts ) => {
        onFetchEnd();
        render( posts.data );
        loadMore( page, posts.max_pages );
      })
      .catch( () => {
        onFetchEnd();
        postsContainer.innerHTML = 'An error ocurred. Please try again by reloading the page.';
        postsContainer.classList.add( noTextClass );
      });
  }

  /**
   * Dom elements configurations after the fetch finishes.
   */
  function onFetchEnd() {
    loader.classList.remove( loaderActiveClass );
  }

  /**
   * Creates the API url
   *
   * @param {number} page The pagination, page number
   * @param {string} department The selected department
   *
   * @return {string} api url
   */
  function createApiUrl( page, department ) {
    window.madison = 'madison' in window ? window.madison : {};
    let apiURL = `${window.madison.api_url}team-members?page=${page}`;
    if ( department && 'all' !== department ) {
      apiURL = `${apiURL}&department=${department}`;
    }
    return apiURL;
  }

  /**
   * Clears container and shows posts.
   *
   * @param {string} department The selected department
   */
  function filterPosts( department ) {
    pageToLoad = 1;
    clearContainer( fetchPosts, pageToLoad, department );
  }

  /**
   * Display transactions on the page
   *
   * @param {Array} posts Array of Posts in JSON
   */
  function render( posts ) {
    if ( posts.length > 0 ) {
      postsContainer.insertAdjacentHTML( 'beforeend', posts.join( ' ' ) );    
      postsContainer.classList.remove( noTextClass );
    } else {
      postsContainer.innerHTML = 'No Team Members Found.';
      postsContainer.classList.add( noTextClass );
    }
  }

  /**
   * Clear the container.
   *
   * @param {function} callback function
   * @param {number} pageToLoad The pagination, page number
   * @param {string} department The selected department
   */
  function clearContainer( callback, pageToLoad, department ) {
    postsContainer.innerHTML = '';
    callback( pageToLoad, department );
  }

  /**
   * Toggle the Load More button and save the next page to load variable.
   *
   * @param {number} actualPage The actual page
   * @param {number} totalPages The total pages of Posts
   */
  function loadMore( actualPage, totalPages ) {
    if ( actualPage < totalPages ) {
      loadMoreButton.classList.add( loadMoreButtonActiveClass );
      const nextPage = pageToLoad + 1;
      if ( nextPage <= totalPages ) {
        pageToLoad = nextPage;
      }
    } else {
      loadMoreButton.classList.remove( loadMoreButtonActiveClass );
    }
  }

  /**
   * Add the active class to the clicked tab.
   *
   * @param {element} tab - the tab clicked.
   */
  function setActiveTab( tab ) {
    tabs.forEach( ( tab ) => tab.classList.remove( activeTabClass ) );
    tab.classList.add( activeTabClass );
  }
}

module.exports = teamTabs;