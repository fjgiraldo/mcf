/**
 * Modal functionnality in Teams module.
 */
function teamModal() {
  const section = document.querySelector( '.o__team' );
  if ( !section ) {
    return;
  }

  const modal = section.querySelector( '.m__team-modal' );
  if ( !modal ) {
    return;
  }

  const mask = section.querySelector( '.m__team-modal-mask' );
  const modalCloseButton = modal.querySelector( '.m__team-modal-close' );
  const image = modal.querySelector( '.m__team-modal-image' );
  const name = modal.querySelector( '.m__team-modal-name' );
  const title = modal.querySelector( '.m__team-modal-title' );
  const department = modal.querySelector( '.m__team-modal-department' );
  const email = modal.querySelector( '.m__team-modal-email' );
  const linkedin = modal.querySelector( '.m__team-modal-linkedin' );
  const bio = modal.querySelector( '.m__team-modal-bio' );

  /**
   * Open Modal on team box click.
   * Needs dynamic binding for the team boxes loaded via Ajax.
   */
  $( section ).on( 'click', '.m__team-box', function openModal() {
    const box = $( this )[ 0 ];
    activateModal( box );
  });

  modalCloseButton.addEventListener( 'click', toggleClasses );    

  /**
   * Toggle the modal opening and closing classes.
   */
  function toggleClasses() {
    const activateModalClass = 'm__team-modal--activate';
    const activateMaskClass = 'm__team-modal-mask--activate';
    const fixBodyClass = 'no-scroll';

    if ( modal.classList.contains( activateModalClass ) ) {
      modal.classList.remove( activateModalClass );
      mask.classList.remove( activateMaskClass );    
      document.body.classList.remove( fixBodyClass );   
    } else {
      modal.classList.add( activateModalClass );
      mask.classList.add( activateMaskClass );    
      document.body.classList.add( fixBodyClass );   
    } 
  }

  /**
   * Shows the team member modal of the member box clicked.
   *
   * @param {element} box - the box clicked.
   */
  function activateModal( box ) {
    toggleClasses();
    setData( box );
  }

  /**
   * Sets the data of the clicked member into the modal.
   *
   * @param {element} box - the box element clicked.
   */
  function setData( box ) {
    const imageFrom = box.querySelector( '.m__team-box-color_picture' );
    if ( imageFrom ) {
      image.src = imageFrom.src;
    }
    const nameFrom = box.querySelector( '.m__team-box-name' );
    if ( nameFrom ) {
      name.innerHTML = nameFrom.innerHTML;
    }
    if ( imageFrom && nameFrom ) {
      image.alt = nameFrom.innerHTML;
    }
    const titleFrom = box.querySelector( '.m__team-box-title' );
    if ( titleFrom ) {
      title.innerHTML = titleFrom.innerHTML;
    }
    const departmentFrom = box.querySelector( '.m__team-box-department' );
    if ( departmentFrom ) {
      department.innerHTML = departmentFrom.innerHTML;
    }
    const emailFrom = box.querySelector( '.m__team-box-email' );
    if ( emailFrom ) {
      email.href = emailFrom.href;
    }
    const linkedinFrom = box.querySelector( '.m__team-box-linkedin' );
    if ( linkedinFrom ) {
      linkedin.href = linkedinFrom.href;
    }
    const bioFrom = box.querySelector( '.m__team-box-bio' );
    if ( bioFrom ) {
      bio.innerHTML = bioFrom.innerHTML;
    }    
  }
}

module.exports = teamModal;