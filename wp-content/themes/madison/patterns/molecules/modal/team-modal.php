<div class="m__team-modal-mask"></div>
<div class="m__team-modal">
	<button class="m__team-modal-close"><?php use_icon( 'close-button-blue', 'm__team-modal-close-icon' ); ?></button>
	<div class="m__team-modal-header">
		<img class="m__team-modal-image" alt="Profile image" src="">
		<div class="m__team-modal-info">
			<div class="m__team-modal-name"></div>
			<div class="m__team-modal-title"></div>
			<div class="m__team-modal-department"></div>
			<div class="m__team-modal-info-footer">
				<a class="m__team-modal-email"><?php use_icon( 'email-icon-navy', 'm__team-modal-email-icon' ); ?></a>
				<a class="m__team-modal-linkedin" target="_blank"><?php use_icon( 'linkedin-circle', 'm__team-modal-linkedin-icon' ); ?></a> 
			</div>
		</div>
	</div>
	<div class="m__team-modal-bio"></div>
</div>
