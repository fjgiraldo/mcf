const from = require( 'array.from' );

/**
 * View More rows.
 */
function listViewMore() {
  const sectionContainer = document.querySelector( '.m__two-col-list' );
  if ( !sectionContainer ) {
    return;
  }

  const viewMoreButton = sectionContainer.querySelector( '.a__btn' );
  const rows = from( sectionContainer.querySelectorAll( '.m__two-col-list-row' ) );
  const rowsShownInitially = 4;
  const hideRowClass = 'm__two-col-list-row--hidden';
  const showButtonClass = 'a__btn--activate';
  let viewFlag = 'more';

  main();

  /**
   * Main function that hides and shows the rows.
   */
  function main() {
    if ( rows.length <= rowsShownInitially ) {
      return;
    }

    viewMoreButton.classList.add( showButtonClass );

    rows.forEach( ( row, index ) => {
      hideRows( row, index );
    });  

    viewMoreButton.addEventListener( 'click', () => {
      rows.forEach( ( row, index ) => {
        if ( 'more' === viewFlag ) {
          row.classList.remove( hideRowClass );
        } else {
          hideRows( row, index );
        }
      });

      if ( 'more' === viewFlag ) {
        viewFlag = 'less';
        viewMoreButton.innerHTML = 'view less';
      } else {
        viewFlag = 'more';
        viewMoreButton.innerHTML = 'view more';
      }      
    });    
  }

  /**
   * Hides the rows.
   *
   * @param {element} row - the row element.
   * @param {number} index - the row index.
   */
  function hideRows( row, index ) {
    if ( index >= rowsShownInitially ) {
      row.classList.add( hideRowClass );
    }    
  }
}

module.exports = listViewMore;