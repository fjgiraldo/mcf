<?php
use Lean\Load;
use Madison\Modules\Verticals\Verticals as Vertical;

$args = wp_parse_args( $args, [
	'posts' => [],
	'bg_color' => 'blue',
]);

if ( empty( $args['posts'] ) ) {
	return;
}
?>
<div class="m__two-col-list m__two-col-list--<?php echo esc_attr( $args['bg_color'] ); ?>">
	<?php foreach ( $args['posts'] as $post ) : ?>
		<?php $col_label = $post['col_label']; ?>
		<a class="m__two-col-list-row" href="<?php the_permalink( $post['id'] ); ?>">
			<span class="m__two-col-list-row-title fade"><?php echo esc_html( $post['title'] ); ?></span>
			<span class="m__two-col-list-row-vertical fade"><?php echo esc_html( $post[ $col_label ] ); ?></span>
			<span class="m__two-col-list-row-icon fade">
				<?php
				$icon_color = 'blue' === $args['bg_color'] ? 'white' : 'lightblue';
				use_icon( 'side-arrow-' . $icon_color, 'm__two-col-list-row-icon-svg' );
				?>
			</span>
		</a>
	<?php endforeach; ?>

	<div class="m__two-col-list-button">
	<?php
	Load::atom( 'buttons/button', [
		'label' => 'Load More',
		'type' => 'blue' === $args['bg_color'] ? 'primary' : 'secondary',
	]);
	?>
	</div>

</div>
