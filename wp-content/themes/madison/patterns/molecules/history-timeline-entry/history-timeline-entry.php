<?php
use Lean\Load;

$args = wp_parse_args( $args, [
	'year' => '',
	'title' => '',
	'description' => '',
	'image' => '',
	'bg_color' => 'blue',
]);

if ( empty( $args['year'] ) ) {
	return;
}
?>

<div class="m__history-timeline-entry" data-hash="<?php echo esc_attr( $args['year'] ); ?>">
	<div class="m__history-timeline-entry-year"><?php echo esc_html( $args['year'] ); ?></div>
	<div class="m__history-timeline-entry-title"><?php echo esc_html( $args['title'] ); ?></div>
	<div class="m__history-timeline-entry-content">
		<?php if ( $args['image'] ) : ?>
			<img class="m__history-timeline-entry-content-image" src="<?php echo esc_url( $args['image'] ); ?>" alt="<?php echo esc_html( $args['title'] ); ?>">
		<?php endif; ?>
		<div class="m__history-timeline-entry-content-description"><?php echo wp_kses_post( $args['description'] ); ?></div>
	</div>
</div>
