'use strict';

/**
 * Function that activate the Search clases used on the animation.
 */
function searchFormAnimation() {
  const headerContainer = document.querySelector( '.o__header' );
  if ( !headerContainer ) {
    return;
  }

  const menu = headerContainer.querySelector( '.m__primary-nav .m__primary-nav-container' );
  const hamburger = headerContainer.querySelector( '.a__hamburger' );
  const iconSearch = headerContainer.querySelector( '.m__icon-search' );
  const searchContainer = document.querySelector( '#search-container' );
  if ( !iconSearch || !searchContainer ) {
    return;
  }

  const searchInput = searchContainer.querySelector( '#search' );
  const searchCloseButton = searchContainer.querySelector( '.search-close-button' );
  const eventOptions = { passive: true };
  const fixsearchContainerClass = 'no-scroll';
  const activeClass = 'active';
  const menuActiveClass = 'is-active';

  iconSearch.addEventListener( 'click', onSearchBoxToggle, eventOptions );
  searchCloseButton.addEventListener( 'click', onSearchBoxToggle, eventOptions );

  /**
   * Click listener function for toggling the search section.
   */
  function onSearchBoxToggle() {
    if ( searchContainer.classList.contains( activeClass ) ) {
      // On search close
      // Close search container.
      searchContainer.classList.remove( activeClass );
      // Toggle search icon.
      iconSearch.classList.remove( activeClass );   
      // Add body scroll.
      document.body.classList.remove( fixsearchContainerClass );     
    } else {
      // On search open
      // Open search container.
      searchContainer.classList.add( activeClass );
      // Toggle search icon.
      iconSearch.classList.add( activeClass );   
      // Remove body scroll.
      document.body.classList.add( fixsearchContainerClass );  
      // Close mobile navigation
      menu.classList.remove( menuActiveClass );  
      hamburger.classList.remove( menuActiveClass );
      // Give focus to the search input text.
      searchContainer.addEventListener( 'animationend', onSearchBoxAnimationEnd, eventOptions );  
    }

  }

  /**
   * Search container Animation End listener.
   */
  function onSearchBoxAnimationEnd() {
    // Give focus to the search input text.
    searchInput.focus();
    // Remove Search container listener.
    searchContainer.removeEventListener( 'animationend', onSearchBoxAnimationEnd, eventOptions );
  }
}

module.exports = searchFormAnimation;