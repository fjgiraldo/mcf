/**
 * Search form focus functionnality.
 */
function searchForm() {
  const searchInput = document.querySelector( '.m__search-form-input' );
  if ( !searchInput ) {
    return;
  }  

  const form = document.querySelector( '.m__search-form-form' );
  const searchLabel = 'SEARCH';
  const searchFocusClass = 'focused';

  form.addEventListener( 'submit', ( event ) => {
    event.preventDefault();
  });

  searchInput.addEventListener( 'focus', () => {
    if ( searchInput.value === searchLabel ) {
      searchInput.value = '';
      searchInput.classList.add( searchFocusClass );
    }
  });

  searchInput.addEventListener( 'focusout', () => {
    if ( searchInput.value === '' ) {
      searchInput.value = searchLabel;
      searchInput.classList.remove( searchFocusClass );
    }
  });
}

module.exports = searchForm;