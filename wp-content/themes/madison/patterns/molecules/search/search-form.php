<?php
use Lean\Load;
use Madison\Modules\Posts\Posts;

$args = wp_parse_args( $args, [
	'class' => '',
]);
?>

<div class="m__search-form">
	<form class="m__search-form-form">
		<input type="text" value="SEARCH" class="m__search-form-input <?php echo esc_attr( $args['class'] ); ?>">
		<?php Load::atom( 'buttons/search-button' ); ?>
	</form>
</div>
