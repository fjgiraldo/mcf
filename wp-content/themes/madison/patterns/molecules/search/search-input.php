<section class="m__search-input" id="search-container">
	<div class="container">
    <?php use_icon( 'close-button-white', 'search-close-button' ); ?>
		<div class="container">
			<form action="/" method="get">
				<input type="text" name="s" id="search" placeholder="Search here..." value="<?php the_search_query(); ?>" />
				<input type="submit" value="Search" class="a__btn a__btn__primary" />
			</form>
		</div>
	</div>
</section>
