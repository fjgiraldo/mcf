const ScrollMagic = require( 'scrollmagic' );

import 'element-closest';
import 'whatwg-fetch';
import 'owl.carousel';
import 'selectric';
import Promise from 'promise-polyfill';
import ready from './templates/shared/ready';
import analytics from './templates/shared/analytics';
import setApiHeaders from './atoms/api-auth/api-auth';
import setFadeAnimation from './atoms/animations/fade-animation';
import openMobileMenu from './atoms/hamburger/hamburger';
import showVideo from './atoms/media/media';
import forms from './atoms/forms/forms';
import backButton from './atoms/buttons/back-button';
import backTop from './atoms/buttons/back-top';
import mainNavigation from './molecules/navigation/primary-nav';
import searchFormAnimation from './molecules/search/search';
import searchForm from './molecules/search/search-form';
import hoverIcon from './molecules/link/link';
import fixHeaderOnScroll from './organisms/header/header';
import hero from './organisms/hero/hero';
import attributesSlider from './organisms/attributes/attributes';
import animateNumbers from './organisms/facts-figures/facts-figures';
import historyTimeline from './organisms/history-timeline/history-timeline';
import transactionsHighlightsSlider from './organisms/transactions-highlights/transactions-highlights';
import awardsSlider from './organisms/awards/awards';
import teamTabs from './molecules/tabs/team-tabs';
import teamModal from './molecules/modal/team-modal';
import shareLink from './molecules/share/share-buttons';
import listViewMore from './molecules/list/list-view-more';
import displayTransactions from './organisms/transactions/transactions';
import processSlider from './organisms/process/process';
import imageCarousel from './organisms/image-carousel/image-carousel';
import selectIndustryAreas from './organisms/sectors/sectors';
import slideshow from './organisms/slideshow/slideshow';
import displayMedia from './organisms/media/media';
import displayInsights from './organisms/insights/insights';

if ( !window.Promise ) {
  window.Promise = Promise;
}

/**
 * Function that is fired once the page is Reaady to fire any JS, add anything
 * required to be executed after the page is ready.
 */
function onReady() {
  window.madison = 'madison' in window ? window.madison : {};
  window.madison.controller = new ScrollMagic.Controller();  
 
  analytics();
  setFadeAnimation();
  setApiHeaders();
  openMobileMenu();
  mainNavigation();
  fixHeaderOnScroll();
  searchFormAnimation();
  searchForm();
  forms();
  hero();
  showVideo();
  attributesSlider();
  animateNumbers();
  transactionsHighlightsSlider();
  awardsSlider();
  hoverIcon();
  historyTimeline();
  teamTabs();
  teamModal();
  listViewMore();
  displayTransactions();
  processSlider();
  imageCarousel();
  selectIndustryAreas();
  slideshow();
  displayMedia();
  displayInsights();
  shareLink();
  backTop();
  backButton();
}

// The callback is fired when the dom is ready.
ready( onReady );
