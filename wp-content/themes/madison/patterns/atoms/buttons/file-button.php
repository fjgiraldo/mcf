<?php
use Lean\Load;

$args = wp_parse_args($args, [
	'label' => 'Download One-Pager',
	'file' => '',
	'type' => 'primary',
]);

if ( empty( $args['file'] ) ) {
	return;
}
?>

<a href="<?php echo esc_url( $args['file'] ); ?>"
	target="_blank" rel="noreferrer noopener"
	class="a__btn a__btn__<?php echo esc_attr( $args['type'] ); ?> a__btn__file">
	<?php
	echo esc_html( $args['label'] );

	Load::atom( 'icons/file-icon', [
		'color' => 'secondary' === $args['type'] ? 'blue' : 'white',
	]);
	?>
</a>
