/**
 * Function that send the user to the previous page using the browser history.
 */
function backButton() {
  const button = document.querySelector( '.a__btn__back' );
  if ( !button ) {
    return;
  }

  button.addEventListener( 'click', () => { 
    history.back();
  });
}

export default backButton;