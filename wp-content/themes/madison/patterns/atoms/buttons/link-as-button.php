<?php
$args = wp_parse_args($args, [
	'link' => [],
  'type' => 'primary',
	'wrapper' => false,
]);

$args_link = wp_parse_args($args['link'], [
  'url' => '',
  'title' => 'Go',
  'target' => '',
]);

if ( empty( $args_link['url'] ) ) {
	return;
}
?>

<?php if ( $args['wrapper'] ) : ?>
  <div class="a__btn-wrapper">
<?php endif; ?>

<a href="<?php echo esc_url( $args_link['url'] ); ?>"
  <?php if ( $args_link['target'] ) : ?>target="_blank" rel="noreferrer noopener"<?php endif; ?>
	class="a__btn a__btn__<?php echo esc_attr( $args['type'] ); ?>">
	<?php echo esc_html( $args_link['title'] ); ?>
</a>

<?php if ( $args['wrapper'] ) : ?>
  </div>
<?php endif; ?>
