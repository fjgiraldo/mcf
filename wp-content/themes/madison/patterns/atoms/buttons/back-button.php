<?php
$args = wp_parse_args($args, [
	'class' => '',
]);
?>

<button class="a__btn__back <?php echo esc_attr( $args['class'] ); ?>">
	<?php use_icon( 'side-arrow-lightblue', 'a__btn__back-svg' ); ?>
	Back
</button>
