const breakpoints = require( 'atoms/base/breakpoints' );
const ScrollMagic = require( 'scrollmagic' );

/**
 * Function that sends the user back to the top of the page.
 */
function backTop() {
  const button = document.querySelector( '.a__btn__back-top' );
  if ( !button ) {
    return;
  }

  const $body = $( 'body, html' );

  button.addEventListener( 'click', () => {
    $body.animate({
      scrollTop: 0
    }, 2000 );
  });

  let offset = 40;

  if ( window.innerWidth > breakpoints.tablet ) {
    offset = 80;
  } 

  // Show back button on second section.
  new ScrollMagic.Scene({ offset: window.innerHeight - offset })
  .setClassToggle( button, 'active' )
  .addTo( window.madison.controller ); 

  const footer = document.querySelector( '.o__footer' );
  if ( !footer ) {
    return;
  }

  // Show back button on footer (if page is too small on height).
  new ScrollMagic.Scene({ triggerElement: footer, triggerHook: 1 })
  .addTo( window.madison.controller )
  .on( 'enter leave', ( event ) => {
    if ( !button.classList.contains( 'active' ) && event.target.controller().info( 'scrollDirection' ) === 'FORWARD' ) {
      button.classList.add( 'active' );
    }
  });   
}

export default backTop;