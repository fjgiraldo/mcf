<?php
$args = wp_parse_args($args, [
	'class' => '',
]);
?>
<button class="a__btn__back-top <?php echo esc_attr( $args['class'] ); ?>">
	<?php use_icon( 'back-to-top-btn', 'a__btn__back-top-svg' ); ?>
</button>
