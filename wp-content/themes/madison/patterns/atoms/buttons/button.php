<?php
$args = wp_parse_args($args, [
	'label' => '',
	'type' => 'primary',
  'class' => '',
] );

if ( empty( $args['label'] ) ) {
  return;
}
?>

<button class="a__btn a__btn__<?php echo esc_attr( $args['type'] ) . ' ' . esc_attr( $args['class'] ); ?>">
	<?php echo esc_html( $args['label'] ); ?>
</button>

