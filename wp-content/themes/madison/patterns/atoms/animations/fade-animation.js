const from = require( 'array.from' );
const ScrollMagic = require( 'scrollmagic' );

/**
 * Function that adds a css class that fades the element when the element is reached. 
 */
function setFadeAnimation() {
  // For every element with fade class
  const elements = from( document.querySelectorAll( '.fade' ) );

  // Adds the scroll magic scenes to all elements found.
  elements.forEach( addScrollingScene );

  /**
   * Function that adds the scroll magic scene.
   * 
   * @param {element} element the element to fade
   */
  function addScrollingScene( element ) {
    if ( !element.hasScene ) {
      element.scene = new ScrollMagic.Scene({ triggerElement: element, triggerHook: 0.9 })
        .setClassToggle( element, 'fade-animation' )
        .addTo( window.madison.controller );
      element.hasScene = true;
    }
  }
}

module.exports = setFadeAnimation;
