const from = require( 'array.from' );

/**
 * The JS functionnality for the forms.
 */
export default function forms() {
  const section = document.querySelector( '.gform_wrapper' );
  if ( !section ) {
    return;
  }

  addSelectedFileTextOnSelect();
  addSelectedFileTextOnSubmitError();

  /**
   * Changes the label text to the selected file name.
   */
  function addSelectedFileTextOnSelect() {
    const inputFiles = from( section.querySelectorAll( '.ginput_container_fileupload input' ) );

    inputFiles.forEach( ( inputFile ) => {
      inputFile.addEventListener( 'change', () => {
        const fileSelectedText = inputFile.parentNode.querySelector( '.gform_input_file_selected' );
        fileSelectedText.classList.add( 'file-selected' );
        fileSelectedText.innerHTML = inputFile.value.split( '\\' ).pop();
      });
    });
  }

  /**
   * If the form has been submitted bu there are errors then show the file name.
   */
  function addSelectedFileTextOnSubmitError() {
    const subSection = document.querySelector( '.gform_validation_error' );
    if ( !subSection ) {
      return;
    }

    const inputFilesContainers = from( subSection.querySelectorAll( '.ginput_container_fileupload' ) );

    inputFilesContainers.forEach( ( inputFilesContainer ) => {
      const fileName = inputFilesContainer.querySelector( '.ginput_preview strong' );

      if ( fileName ) {
        const fileSelectedText = inputFilesContainer.querySelector( '.gform_input_file_selected' );
        fileSelectedText.classList.add( 'file-selected' );
        fileSelectedText.innerHTML = fileName.innerHTML;
      }
    });
  }
}