/**
 * Function that opens the mobile menu when clicking the hamburger
 */
function openMobileMenu() {
  const activeClass = 'is-active';
  const fixMenuClass = 'no-scroll';
  const hamburger = document.querySelector( '.a__hamburger' );
  const menu = document.querySelector( '.m__primary-nav .m__primary-nav-container' );

  if ( !hamburger || !menu ) {
    return;
  }

  hamburger.addEventListener( 'click', toggle );

  /**
   * Function that is called every time the hamburger element is clicked.
   */
  function toggle() {
    if ( hamburger.classList.contains( activeClass ) ) {
      hamburger.classList.remove( activeClass );
      menu.classList.remove( activeClass );
      document.body.classList.remove( fixMenuClass );
    } else {
      hamburger.classList.add( activeClass );
      menu.classList.add( activeClass );
      document.body.classList.add( fixMenuClass );      
    }

  }  
}

export default openMobileMenu;