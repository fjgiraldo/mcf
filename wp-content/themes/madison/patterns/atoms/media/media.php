<?php
$args = wp_parse_args( $args, [
  'title' => '',
  'type' => 'none',
  'image' => '',
  'video' => '',
  'video_brightcove' => '',
  'video_image' => '',
]);

if ( ! $args['type'] || 'none' === $args['type'] ) {
  return;
}
?>

<div class="a__media a__media--<?php echo esc_attr( $args['type'] ); ?> <?php if ( $args['video_image'] ) : ?>a__media-has-video-placeholder<?php endif; ?>">

  <?php if ( 'image' === $args['type'] ) : ?>
		<img class="a__media-image" src="<?php echo esc_attr( $args['image'] ); ?>">
  <?php else : ?>

		<?php if ( 'video' === $args['type'] ) : ?>
			<div class="a__media-video">
			<?php echo wp_kses( $args['video'], [
				'iframe' => [
					'src' => [],
					'height' => [],
					'width' => [],
					'frameborder' => [],
					'allowfullscreen' => [],
				],
			]); ?>
			</div>
		<?php else : ?>
			<iframe class="a__media-video a__media-video--brightcove" data-video="<?php echo esc_attr( $args['video_brightcove'] ); ?>" allowfullscreen webkitallowfullscreen mozallowfullscreen></iframe>
		<?php endif; ?>

		<?php if ( $args['video_image'] ) : ?>
			<img class="a__media-video-image" src="<?php echo esc_attr( $args['video_image'] ); ?>" alt="<?php echo esc_attr( $args['title'] ); ?>">
			<?php use_icon( 'play-button', 'a__media-video-icon' ); ?>
		<?php endif; ?>

	<?php endif; ?>

</div>
