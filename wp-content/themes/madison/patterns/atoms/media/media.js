/**
 * Function that activate the video when the image video or icon are clicked.
 */
function showVideo() {
  const section = document.querySelector( '.a__media' );
  if ( !section ) {
    return;
  }

  const isEmbeddedVideo = section.classList.contains( 'a__media--video' );
  const isBrightcoveVideo = section.classList.contains( 'a__media--brightcove' );
  if ( !isEmbeddedVideo && !isBrightcoveVideo ) {
    return;
  }

  const playButton = section.querySelector( '.a__media-video-icon' );
  const video = section.querySelector( '.a__media-video' );
  const imageVideo = section.querySelector( '.a__media-video-image' );
  const eventOptions = { passive: true };

  imageVideo.addEventListener( 'click', onPlayButtonClick, eventOptions );
  playButton.addEventListener( 'click', onPlayButtonClick, eventOptions );

  /**
   * Click listener function for showing the video.
   * If its a Brightcove video it will added to the iframe (this improves page loading time)
   */
  function onPlayButtonClick() {
    if ( !section.classList.contains( 'active' ) ) {
      section.classList.add( 'active' );
      if ( isBrightcoveVideo ) {
        video.src = video.dataset.video;
      }
      playButton.removeEventListener( 'click', onPlayButtonClick, eventOptions );
    }
  }
}

module.exports = showVideo;