const base64 = require( 'base-64' );

/**
 * Function that set the headers for the Api calls.
 */
function setApiHeaders() {
  window.madison = 'madison' in window ? window.madison : {};
  const stagingAuth = 'staging_auth' in window.madison ? window.madison.staging_auth : {};
  const headers = new Headers();
  if ( stagingAuth.username && stagingAuth.password ) {
    const credentials = `${stagingAuth.username}:${stagingAuth.password}`;
    headers.append( 'Authorization', `Basic ${base64.encode( credentials )}` );
  }
  const apiHeaders = {
    method: 'GET',
    headers: headers,
  };
  
  window.madison.apiHeaders = apiHeaders;
}

export default setApiHeaders;