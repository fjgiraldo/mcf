const breakpoints = {
  mobile: 425,
  tablet: 768,
  desktop: 1024,
};

module.exports = breakpoints;