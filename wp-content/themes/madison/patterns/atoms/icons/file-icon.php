<?php
$args = wp_parse_args($args, [
	'color' => 'blue',
	'orientation' => 'normal',
	'class' => '',
]);
?>
<span class="a__file-icon a__file-icon--<?php echo esc_attr( $args['orientation'] ) . ' ' . esc_attr( $args['class'] ); ?>">
<?php
use_icon( 'download-btn-' . $args['color'], 'a__file-icon-svg-normal' );
use_icon( 'download-btn-white', 'a__file-icon-svg-hover' );
?>
</span>
