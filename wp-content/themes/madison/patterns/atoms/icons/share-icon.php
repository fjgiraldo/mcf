<?php
$args = wp_parse_args($args, [
	'social-media' => '',
	'class' => '',
]);

if ( empty( $args['social-media'] ) ) {
	return;
}
?>
<span class="a__share-icon <?php echo esc_attr( $args['class'] ); ?>">
	<?php use_icon( $args['social-media'] . '-share', 'a__share-icon-svg-normal' ); ?>
	<?php use_icon( $args['social-media'] . '-hover', 'a__share-icon-svg-hover' ); ?>
</span>
