<?php
$args = wp_parse_args($args, [
	'color' => 'white',
	'orientation' => 'normal',
	'class' => '',
]);
?>
<span class="a__circular-arrow-icon a__circular-arrow-icon--<?php echo esc_attr( $args['orientation'] ) . ' ' . esc_attr( $args['class'] ); ?>">
<?php
use_icon( 'circular-arrow-' . $args['color'], 'a__circular-arrow-icon-svg-normal' );
use_icon( 'circular-arrow-' . $args['color'] . '-hover', 'a__circular-arrow-icon-svg-hover' );
?>
</span>
