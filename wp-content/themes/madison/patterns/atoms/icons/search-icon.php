<?php
$args = wp_parse_args($args, [
	'class' => '',
]);
?>
<span class="a__search-icon <?php echo esc_attr( $args['class'] ); ?>">
<?php
use_icon( 'search-icon-gray', 'a__search-icon-svg-normal' );
use_icon( 'search-icon-blue-hover', 'a__search-icon-svg-hover' );
?>
</span>
