<?php
use Lean\Load;

$args = wp_parse_args( $args, [
  'type' => 'section',
  'color' => 'blue',
]);
?>

<div class="a__separator a__separator--<?php echo esc_attr( $args['type'] ); ?> container container--no-padding container--padding-mobile">
	<div class="a__separator-line a__separator-line--<?php echo esc_attr( $args['color'] ); ?>"></div>
</div>
