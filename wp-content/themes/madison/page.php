<?php use Lean\Load; ?>

<?php get_header(); ?>

<main class="main-container default-page-container" role="main" itemprop="mainContentOfPage">

<?php Load::organism( 'page/page' ); ?>

</main>

<?php get_footer(); ?>
