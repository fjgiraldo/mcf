<?php
use Madison\ThemeSetup;
use Madison\Modules\JobOpportunities\JobOpportunities;
use Madison\Modules\DealAnnouncements\DealAnnouncements;

/**
 * Sets up theme defaults and registers support for various WordPress features.
 */

// Constants.
define( 'LEANP_THEME_NAME', 'LeanName' );
define( 'LEANP_THEME_VERSION', '0.1.0' );
define( 'LEANP_MINIMUM_WP_VERSION', '4.3.1' );

// Composer autoload.
require_once get_template_directory() . '/vendor/autoload.php';
require_once get_template_directory() . '/Setup.php';
ThemeSetup::init();

// Run the theme setup.
add_filter( 'loader_directories', function( $directories ) {
	$directories[] = get_template_directory() . '/patterns';
	return $directories;
});

add_filter('loader_alias', function( $alias ) {
	$alias['atom'] = 'atoms';
	$alias['molecule'] = 'molecules';
	$alias['organism'] = 'organisms';
	return $alias;
});


/**
 * Use an icon from an icon sprite.
 *
 * @param string $id The ID of the icon.
 * @param string $class_name The name of the class to be used for the icon.
 */
function use_icon( $id = '', $class_name = '' ) {
?>
	<svg class="<?php echo esc_attr( $class_name ); ?>">
		<use xlink:href="#<?php echo esc_attr( $id ); ?>" />
	</svg>
<?php
}

/**
 * Function used to render custom tags from the admin into the site just after
 * the <body> tag.
 */
add_action( 'lean/before_header', function() {
	if ( function_exists( 'the_field' ) ) {
		the_field( 'general_options_google_tag_manager', 'option' );
	}
});

/**
 * Hook applied to before the header to load the sprite with the icons before
 * loading the sprite makes sure the file is present to prevent any error to
 * happen.
 */
add_action( 'lean/after_footer', function() {
	$icon_path = './patterns/static/icons/icons.svg';
	$sprite = get_theme_file_path( $icon_path );
	// @codingStandardsIgnoreStart
	$content = file_exists( $sprite ) ? file_get_contents( $sprite ) : false;
	// @codingStandardsIgnoreEnd
	if ( $content ) {
		// @codingStandardsIgnoreStart
		printf( '<div class="visuallyhidden">%s</div>', $content );
		 // @codingStandardsIgnoreEnd
	}
});

add_action( 'after_setup_theme', function() {
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'html5' );
});

/**
 * Namespace used for the localize object.
 */
add_filter( 'lean_assets_localize_script', function() {
	return 'madison';
});

/**
 * Localize the dynamic data that is going to be available for the Front End or
 * via the browser.
 */
add_filter( 'lean_assets_localize_data', function() {
	return [
		'staging_auth' => [
			'username' => esc_js( get_field( 'flywheel_username', 'options' ) ),
			'password' => esc_js( get_field( 'flywheel_password', 'options' ) ),
		],
		'api_url' => '/wp-json/madison/v1/',
	];
});

/**
 * Theme image sizes.
 */
add_action( 'after_setup_theme', function() {
	// Mobile image size to use in img srcset.
	add_image_size( 'mobile', 425, 9999 );
	add_image_size( 'container', 955, 9999 );
	add_image_size( 'full-screen', 1800, 9999 );
});

/**
 * Add page name as class in body tag.
 */
add_filter( 'body_class', function( $classes ) {
	if ( get_the_title() ) {
		$classes[] = 'page-slug-' . strtolower( get_the_title() );
	}
	return $classes;
});

/**
 * Add a class to the body tag on confirmation page.
 */
add_action( 'gform_after_submission', function() {
	add_filter( 'body_class', function( $classes ) {
		$classes[] = 'gform_form_submitted';
		return $classes;
	});
}, 10, 2 );

/**
 * Adds the html label tag next to the input file to set custom style in Gravity Forms.
 */
add_filter( 'gform_field_content', function( $field_content, $field, $value, $lead_id, $form_id ) {
	if ( 'fileupload' === $field->type ) {
		$field_id = 'input_' . $form_id . '_' . $field->id;
		// Find the string position inmediately after the input file.
		$position = strpos( $field_content, '<span id' );
		if ( $position ) {
			// Add label in string position.
			$field_content = substr_replace(
				$field_content,
				'<label class="gform_input_file_label" for="' . $field_id . '">' . $field->label . '</label><div class="gform_input_file_selected">No File Selected</div>',
				$position,
				0
			);
		}
	}
	return $field_content;
}, 10, 5 );

/*
* Enable <br> breaks in all esc_html().
*/
add_filter( 'esc_html', function( $safe_text ) {
	return str_replace( '&lt;br&gt;', '<br>', $safe_text );
});

/*
* Adds a script inside the HEAD tag.
*/
add_action( 'wp_head', function() {
	if ( function_exists( 'the_field' ) ) {
		the_field( 'general_options_head_script', 'option' );
	}
});

/**
 * Change the default "sizes" srcset attribute.
 */
add_filter( 'wp_calculate_image_sizes', function( $sizes, $size, $image_src, $image_meta, $attachment_id ) {
	$image_full_metadata = (array) wp_get_attachment_metadata( $attachment_id );
	$half_width = $image_full_metadata['width'] / 2;
	$sizes = '(min-width: 2048px) ' . $image_full_metadata['width'] . 'px,';
	$sizes .= '(min-width: 1024px) ' . $half_width . 'px,';
	$sizes .= '(min-width: 850px) 850px,';
	$sizes .= '(min-width: 425px) 425px';
	return $sizes;
}, 10, 5 );

/* Remove archives by setting to 404 not found page*/
add_action( 'template_redirect', function() {
	// If we are on category or tag or date or author archive.
	if ( is_category() || is_tag() || is_date() || is_author() ) {
		global $wp_query;
		$wp_query->set_404();
	}
});

// Add the Transaction date custom column to the Transactions post type.
add_filter( 'manage_transaction_posts_columns', function( $columns ) {
	return [
		'title' => 'Title',
		'transaction_date' => 'Transaction Date',
		'taxonomy-vertical' => 'Vertical',
		'date' => 'Date',
	];
});

// Add the data to the Transaciton date custom column for the Transaction post type.
add_action( 'manage_transaction_posts_custom_column' , function( $column, $post_id ) {
	if ( 'transaction_date' === $column ) {
		the_field( 'transactions_date', $post_id );
	}
}, 10, 2 );

// Saves transaction date modifying the day to the first of the month.
// This is done to order the Transactions correctly by month/year/menu_order.
add_action( 'save_post_transaction', function( $id, $post ) {
	$transaction_date = get_field( 'transactions_date', $id );
	if ( $transaction_date ) {
		$date = new DateTime( $transaction_date );
		$date->modify( 'first day of this month' );
		update_field( 'transactions_date', $date->format( 'Ymd' ), $id );
	}
}, 10, 2);
