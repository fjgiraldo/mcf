<?php
use Lean\Load;
use Madison\Modules\Posts\Posts;
?>

<?php get_header(); ?>

<main class="main-container default-page-container" role="main" itemprop="mainContentOfPage">

	<div class="container">
	<?php
	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post();
			Load::organism( 'post/post', Posts::get_data( get_the_id() ) );
		}
	}
	?>
	</div>

</main>

<?php get_footer(); ?>
