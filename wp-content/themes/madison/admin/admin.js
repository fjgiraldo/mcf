jQuery( document ).ready( function( $ ) {
  // Hide Content Editor on Post Edition page when the post type is not Text.
  var postTypeRadioButtonsContainer = $( '#admin_post_type' );
  var postTypeRadioButtons = $( 'input:radio', postTypeRadioButtonsContainer );

  if ( postTypeRadioButtons.length > 0 ) {
    var editorContainer = $( '.postarea' );

    toggleEditorContainer();

    postTypeRadioButtons.on( 'click', function() {
      toggleEditorContainer();
    });
  }

  function toggleEditorContainer() {
    var checkedButtonValue = $( postTypeRadioButtonsContainer ).find( 'input:radio:checked' ).val();
    if ( 'text' === checkedButtonValue ) {
      editorContainer.show();
    } else {
      editorContainer.hide();
    }    
  }
});