<?php
use \Lean\Load;
use Madison\Helpers;

get_header();
?>
<div class="main-container">
	<div class="search-section container">
		<h1 class="search-section-title">Search Results.</h1>
		<h2 class="search-section-subtitle">You Searched for: <?php the_search_query(); ?></h2>
		<?php if ( have_posts() ) : ?>
			<section class="results-section">
				<input type="hidden" class="query" value="<?php the_search_query(); ?>">
				<?php
				while ( have_posts() ) {
					the_post();
				?>
				<div class="result-item">
					<a 
					class="result-item-title"
					href="<?php the_permalink(); ?>"
					title="<?php the_title(); ?>">
					<?php the_title(); ?>
					</a>
					<p class="result-item-excerpt">
						<?php echo esc_html( Helpers::custom_excerpt( get_the_ID(), 20 ) ); ?>
					</p>
				</div>
				<?php } ?>
			</section>
			<footer class="search-footer">
				<div class="nav-previous alignleft"><?php next_posts_link( 'Older posts' ); ?></div>
				<div class="nav-next alignright"><?php previous_posts_link( 'Newer posts' ); ?></div>
			</footer>
		<?php else : ?>
			<p class="no-results">
				No results were found.
			</p>
		<?php endif; ?>
	</div>
</div>

<?php get_footer(); ?>
