<?php namespace Madison\Modules\Verticals;

use Madison\Helpers as Helper;
use Madison\Modules\Transactions\Transactions as Transaction;
use Madison\Modules\TeamMembers\TeamMembers as TeamMember;
use Madison\Modules\DealAnnouncements\DealAnnouncements as DealAnnouncement;

/**
 * Class that creates the verticals taxonomies used in the Posts, Transactions and Team Members types.
 */
class Verticals {
	const TAXONOMY = 'vertical';

	/**
	 * Function called automatically and works as the entry point of the class.
	 */
	public static function init() {
		add_action( 'init', [ __CLASS__, 'register_taxonomy' ] );
	}

	/**
	 * Function called to register a new taxonomy associated with each post
	 */
	public static function register_taxonomy() {
		$category_taxonomy = [
			'public' => false,
			'hierarchical'      => true,
			'labels'            => self::get_labels( 'Vertical', 'Verticals' ),
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => [
				'slug' => 'vertical',
			],
		];
		register_taxonomy( self::TAXONOMY, [ Transaction::TYPE, TeamMember::TYPE, DealAnnouncement::TYPE ], $category_taxonomy );
	}

	/**
	 * Function that helps in the process of the creation of labels for WP.
	 *
	 * @param string $singular The singular representation of the label.
	 * @param string $plural The plural version of the string.
	 * @return array An array with the details of the labels.
	 */
	public static function get_labels( $singular, $plural ) {
		return [
			'name'              => $singular,
			'singular_name'     => $singular,
			'search_items'      => "Search $singular",
			'all_items'         => "All $plural",
			'parent_item'       => "Parent $singular",
			'parent_item_colon' => "Parent $singular:",
			'edit_item'         => "Edit $singular",
			'update_item'       => "Update $singular",
			'add_new_item'      => "Add New $singular",
			'new_item_name'     => "New $singular Name",
			'menu_name'         => "$plural",
		];
	}
}
