<?php namespace Madison\Modules\JobOpportunities;

/**
 * Class that creates a new CPT to storage the Transactions.
 */
class JobOpportunities {
	const TYPE = 'job-opportunity';

	/**
	 * Function called automatically and works as the entry point of the class.
	 */
	public static function init() {
		add_action( 'init', [ __CLASS__, 'register_cpt' ] );
	}

	/**
	 * Register a new CPT to storage the Job Opportunities.
	 */
	public static function register_cpt() {
		$cpt = new \Lean\Cpt([
			'singular' => 'Job Opportunity',
			'plural' => 'Job Opportunities',
			'post_type' => self::TYPE,
			'slug' => self::TYPE,
			'supports' => [
				'title',
				'editor',
			],
			'args' => [
				'menu_icon' => 'dashicons-id',
				'exclude_from_search' => false,
			],
		]);
		$cpt->init();
	}

	/**
	 * Get the data as an array from a Job Opportunities post type.
	 *
	 * @param int $id The post ID number.
	 * @return Array An array with the data of each Job Opportunity.
	 */
	public static function get_data( $id ) {

		return [
			'id' => $id,
			'title' => get_the_title( $id ),
			'location' => get_field( 'job_opportunity_location', $id ),
			'col_label' => 'location',
		];
	}

	/**
	 * Get the all Job Opportunities posts.
	 *
	 * @return Array An array with the data of each Job Opportunity.
	 */
	public static function get_posts() {
		$posts = [];

		$args = [
			'fields' => 'ids',
			'posts_per_page' => 100,
			'post_type' => self::TYPE,
			'order' => 'DESC',
			'no_found_rows' => true,
			'update_post_meta_cache' => false,
			'update_post_term_cache' => false,
			'orderby' => 'menu_order',
			'order' => 'ASC',
		];

		$query = new \WP_Query( $args );

		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
				$query->the_post();
				$posts[] = self::get_data( get_the_id() );
			}
			wp_reset_postdata();
		}
		return $posts;
	}
}
