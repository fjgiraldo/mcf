<?php namespace Madison\Modules\TeamMembers;
use Lean\Load;

/**
 * Class that creates a new CPT to storage the Team Members.
 */
class TeamMembers {
	const TYPE = 'team-member';
	const TAXONOMY = 'department';
	const TAXONOMY_VERTICAL = 'vertical';

	/**
	 * Function called automatically and works as the entry point of the class.
	 */
	public static function init() {
		add_action( 'init', [ __CLASS__, 'register_cpt' ] );
	}

	/**
	 * Register a new CPT to storage the Team Members.
	 */
	public static function register_cpt() {
		$team_members = new \Lean\Cpt([
			'singular' => 'Team Member',
			'plural' => 'Team Members',
			'post_type' => self::TYPE,
			'slug' => 'team-member',
			'supports' => [
				'title',
				'editor',
			],
			'args' => [
				'menu_icon' => 'dashicons-businessman',
				'publicly_queryable' => false,
			],
			'title_placeholder' => 'Name',
		]);
		$team_members->init();
		self::register_taxonomy();
	}

	/**
	 * Function called to register a new taxonomy associated with each transaction
	 */
	public static function register_taxonomy() {
		$category_taxonomy = [
			'public' => false,
			'hierarchical'      => false,
			'labels'            => self::get_labels( 'Department', 'Departments' ),
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => [
				'slug' => 'department',
			],
		];
		register_taxonomy( self::TAXONOMY, [ self::TYPE ], $category_taxonomy );
	}

	/**
	 * Function that helps in the process of the creation of labels for WP.
	 *
	 * @param string $singular The singular representation of the label.
	 * @param string $plural The plural version of the string.
	 * @return array An array with the details of the labels.
	 */
	public static function get_labels( $singular, $plural ) {
		return [
			'name'              => $singular,
			'singular_name'     => $singular,
			'search_items'      => "Search $singular",
			'all_items'         => "All $plural",
			'parent_item'       => "Parent $singular",
			'parent_item_colon' => "Parent $singular:",
			'edit_item'         => "Edit $singular",
			'update_item'       => "Update $singular",
			'add_new_item'      => "Add New $singular",
			'new_item_name'     => "New $singular Name",
			'menu_name'         => "$plural",
		];
	}

	/**
	 * Get the team members by taxonomy term.
	 *
	 * @param array $args An array with the arguments.
	 * @return Array An array with the list of team members.
	 */
	public static function get_members_by_term( $args ) {
		$defaults = [
			'fields' => 'ids',
			'post_type' => self::TYPE,
			'update_post_meta_cache' => false,
			'paged' => 1,
			'posts_per_page' => 100,
			'orderby' => 'menu_order',
			'order' => 'ASC',
		];

		$query = new \WP_Query( wp_parse_args( $args['query'], $defaults ) );
		$total = $query->max_num_pages;
		$members = [];

		foreach ( $query->posts as $post_id ) {
			$data = self::get_data( $post_id );
			$members[] = empty( $args['return_html'] ) ? $data : self::render_box( $data );
		}

		return [
			'data' => $members,
			'max_pages' => $total,
		];
	}

	/**
	 * Get the data as an array from a team member.
	 *
	 * @param int $id The post ID number.
	 * @return Array An array with the data of each team member.
	 */
	public static function get_data( $id ) {
		return [
			'id' => $id,
			'name' => get_the_title( $id ),
			'title' => get_field( 'team_member_title', $id ),
			'bio' => apply_filters( 'the_content', get_post_field( 'post_content', $id ) ),
			'department' => get_field( 'team_member_department', $id ),
			'email' => get_field( 'team_member_email', $id ),
			'linkedin' => get_field( 'team_member_linkedin', $id ),
			'color_picture' => get_field( 'team_member_color_picture', $id ),
			'bw_picture' => get_field( 'team_member_bw_picture', $id ),
		];
	}

	/**
	 * Returns the box html for a post.
	 *
	 * @param array $data An array with all the post data.
	 * @return string The html
	 */
	public static function render_box( $data ) {
		ob_start();
		Load::molecule( 'box/team-box', $data );
		return ob_get_clean();
	}
}
