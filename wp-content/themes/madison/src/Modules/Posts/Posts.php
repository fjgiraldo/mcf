<?php namespace Madison\Modules\Posts;

use Lean\Load;
use Madison\Helpers as Helper;

/**
 * Class that creates the taxonomies and contains some helper classes for the Posts Post Type.
 */
class Posts {
	const TYPE = 'post';
	const EXCERPT_MAX_WORDS = 10;

	/**
	 * Get the data as an array from a post.
	 *
	 * @param int $id The post ID number.
	 * @return Array An array with the data of each transaction.
	 */
	public static function get_data( $id ) {
		$post_type = get_field( 'post_type', $id );
		$show_date = boolval( get_field( 'post_show_published_date', $id ) );
		$category_terms = wp_get_post_terms( $id, 'category', [
			'fields' => 'names',
		]);
		$category = empty( $category_terms[0] ) ? '' : $category_terms[0];

		$link_url = get_the_permalink( $id );

		if ( 'link' === $post_type ) {
			$link_url = get_field( 'post_external_link', $id );
		} elseif ( 'file' === $post_type ) {
			$link_url = get_field( 'post_file_upload', $id );
		}

		return [
			'type' => $post_type,
			'title' => get_the_title( $id ),
			'title_2nd_style' => get_field( 'post_title_style', $id ),
			'content' => apply_filters( 'the_content', get_post_field( 'post_content', $id ) ),
			'excerpt' => get_post_field( 'post_excerpt', $id ),
			'text' => Helper::custom_excerpt( $id, self::EXCERPT_MAX_WORDS ),
			'date' => $show_date ? get_the_date() : false,
			'thumbnail' => get_field( 'post_thumbnail_image', $id ),
			'category' => $category,
			'type' => $post_type,
			'link_url' => $link_url,
			'media_type' => get_field( 'post_media', $id ),
			'image' => get_field( 'post_image', $id ),
			'video' => get_field( 'post_video', $id ),
			'location' => get_field( 'job_opportunity_location', $id ),
			'show_share_buttons' => get_field( 'post_show_share_buttons', $id ),
			'apply_label' => get_field( 'options_apply_label', 'options' ),
			'apply_link' => get_field( 'options_apply_page', 'options' ),
		];
	}

	/**
	 * Function used by the Transactions API to get the data used on the Transactions page.
	 * It filters by year and sector if needed.
	 *
	 * @param array $args An array with all the query parameters.
	 * @return array An array with the results.
	 */
	public function query( $args ) {
		$defaults = [
			'fields' => 'ids',
			'paged' => 1,
			'posts_per_page' => 6,
			'post_type' => self::TYPE,
			'order' => 'DESC',
		];

		$query = new \WP_Query( wp_parse_args( $args, $defaults ) );
		$this->total = $query->max_num_pages;
		$posts = [];

		foreach ( $query->posts as $post_id ) {
			$posts[] = self::render_post_box( self::get_data( $post_id ) );
		}

		return [
			'data' => $posts,
			'max_pages' => $this->total,
		];
	}

	/**
	 * Returns the box html for a post.
	 *
	 * @param array $data An array with all the post data.
	 * @return string The html
	 */
	public static function render_post_box( $data ) {
		$data['thumbnail'] = '';
		ob_start();
		Load::molecule( 'box/media-insight-box', $data );
		return ob_get_clean();
	}

	/**
	 * Returns all years on which posts have been writter.
	 *
	 * @return array The array with the post years
	 */
	public static function get_all_years() {
		$atts = [
			'type'            => 'yearly',
			'limit'           => '',
			'format'          => '',
			'before'          => '',
			'after'           => '',
			'show_post_count' => false,
			'echo'            => 0,
			'order'           => 'DESC',
		];
		// Filter by 4 numbers.
		preg_match_all( '/>([0-9]{4})</', wp_get_archives( $atts ), $matches );
		return empty( $matches ) ? [] : $matches[1];
	}

	/**
	 * Returns all terms from the category taxonomy except uncategorized.
	 *
	 * @return array An array with all terms.
	 */
	public static function get_all_category_terms() {
		$args = [
			'exclude' => 1,
		];
		return get_terms( 'category', $args );
	}
}
