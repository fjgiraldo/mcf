<?php namespace Madison\Modules\DealAnnouncements;

use Madison\Modules\Verticals\Verticals as Vertical;

/**
 * Class that creates a new CPT to storage the Transactions.
 */
class DealAnnouncements {
	const TYPE = 'deal-announcement';

	/**
	 * Total max page on query.
	 *
	 * @var {number} total Total max page on query.
	 */
	public $total = 0;

	/**
	 * Function called automatically and works as the entry point of the class.
	 */
	public static function init() {
		add_action( 'init', [ __CLASS__, 'register_cpt' ] );
	}

	/**
	 * Register a new CPT to storage the Transactions.
	 */
	public static function register_cpt() {
		$deal_announcement = new \Lean\Cpt([
			'singular' => 'Deal Announcement',
			'plural' => 'Deal Announcements',
			'post_type' => self::TYPE,
			'slug' => 'deal-announcement',
			'supports' => [
				'title',
				'editor',
			],
			'args' => [
				'menu_icon' => 'dashicons-megaphone',
				'exclude_from_search' => false,
			],
		]);
		$deal_announcement->init();
	}

	/**
	 * Get the data as an array from a Deal Announcement post type.
	 *
	 * @param int $id The post ID number.
	 * @return Array An array with the data of each Deal Announcement.
	 */
	public static function get_data( $id ) {
		$category_terms = wp_get_post_terms( $id, Vertical::TAXONOMY, [
			'fields' => 'names',
		]);
		$category = empty( $category_terms[0] ) ? '' : $category_terms[0];

		return [
			'id' => $id,
			'title' => get_the_title( $id ),
			'vertical' => $category,
			'col_label' => 'vertical',
		];
	}

	/**
	 * Get the all Deal Announcements posts or by term.
	 *
	 * @param id $term_id The term ID to search for.
	 * @return Array An array with the data of each Deal Announcement.
	 */
	public static function get_posts( $term_id = null ) {
		$posts = [];

		$args = [
			'fields' => 'ids',
			'posts_per_page' => 100,
			'post_type' => self::TYPE,
			'order' => 'DESC',
			'no_found_rows' => true,
			'update_post_meta_cache' => false,
			'update_post_term_cache' => false,
			'orderby' => 'menu_order',
			'order' => 'ASC',
		];

		if ( $term_id ) {
			$args['update_post_term_cache'] = true;
			$args['tax_query'] = [
				[
					'taxonomy' => Vertical::TAXONOMY,
					'terms' => $term_id,
				],
			];
		}

		$query = new \WP_Query( $args );

		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
				$query->the_post();
				$posts[] = self::get_data( get_the_id() );
			}
			wp_reset_postdata();
		}
		return $posts;
	}
}
