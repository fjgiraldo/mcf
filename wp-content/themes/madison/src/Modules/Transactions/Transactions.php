<?php namespace Madison\Modules\Transactions;
use Lean\Load;

use Madison\Modules\Verticals\Verticals as Vertical;
/**
 * Class that creates a new CPT to storage the Transactions.
 */
class Transactions {
	const TYPE = 'transaction';
	const POST_LIMIT = 100;

	/**
	 * Total max page on query.
	 *
	 * @var {number} total Total max page on query.
	 */
	public $total = 0;

	/**
	 * Function called automatically and works as the entry point of the class.
	 */
	public static function init() {
		add_action( 'init', [ __CLASS__, 'register_cpt' ] );
	}

	/**
	 * Register a new CPT to storage the Transactions.
	 */
	public static function register_cpt() {
		$transaction = new \Lean\Cpt([
			'singular' => 'Transaction',
			'plural' => 'Transactions',
			'post_type' => self::TYPE,
			'slug' => 'transaction',
			'supports' => [
				'title',
			],
			'args' => [
				'menu_icon' => 'dashicons-controls-repeat',
				'publicly_queryable' => false,
			],
		]);
		$transaction->init();
	}

	/**
	 * Get the data as an array from a transaction.
	 *
	 * @param int $id The transaction ID number.
	 * @return Array An array with the data of each transaction.
	 */
	public static function get_data( $id ) {
		return [
			'title' => get_the_title( $id ),
			'logo_top' => get_field( 'transactions_logo_top', $id ),
			'quantity' => get_field( 'transactions_deal_amount', $id ),
			'text' => get_field( 'transactions_role', $id ),
			'transactions_type' => get_field( 'transactions_transaction_type', $id ),
			'logo_bottom' => get_field( 'transactions_logo_bottom', $id ),
			'date' => get_field( 'transactions_date', $id ),
		];
	}

	/**
	 * Function used by the Transactions API to get the data used on the Transactions page.
	 * It filters by year and sector if needed.
	 *
	 * @param int $page The number of page to request.
	 * @param int $year The year filter.
	 * @param int $sector The sector filter.
	 * @return array An array with the results.
	 */
	public function query( $page = 1, $year = null, $sector = null ) {
		$args = [
			'fields' => 'ids',
			'paged' => $page,
			'posts_per_page' => 9,
			'post_type' => self::TYPE,
			'meta_key' => 'transactions_date',
			'orderby' => [
				'meta_value_num' => 'DESC',
				'menu_order' => 'ASC',
			],
		];

		if ( $year ) {
			$args['meta_query'] = [
				[
					'key' => 'transactions_date',
					'value' => [ $year . '0101', $year . '1231' ],
					'compare' => 'BETWEEN',
					'type'    => 'NUMERIC',
				],
			];
		}

		if ( $sector ) {
			$args['tax_query'] = [
				[
					'taxonomy' => 'vertical',
					'field' => 'slug',
					'terms' => $sector,
				],
			];
		}

		$query = new \WP_Query( $args );
		$this->total = $query->max_num_pages;
		$posts = [];

		foreach ( $query->posts as $post_id ) {
			ob_start();
			Load::molecule( 'box/transactions-highlights-box', self::get_data( $post_id ) );
			$posts[] = ob_get_clean();
		}

		return [
			'data' => $posts,
			'max_pages' => $this->total,
		];
	}

	/**
	 * Returns all unique transactions years.
	 */
	public static function get_all_years() {
		// @codingStandardsIgnoreStart
		global $wpdb;
		return $wpdb->get_results( "SELECT DISTINCT LEFT(meta_value, 4) as year FROM $wpdb->postmeta pm WHERE meta_key = 'transactions_date'", ARRAY_A );
		// @codingStandardsIgnoreEnd
	}

	/**
	 * Returns all the transaction sectors (vertical taxonomy).
	 * Return only the terms that are assigned to Transactions posts.
	 */
	public static function get_all_verticals() {
		$args = [
			'fields' => 'ids',
			'posts_per_page' => self::POST_LIMIT,
			'post_type' => [
				self::TYPE,
			],
		];

		$query = new \WP_Query( $args );
		$post_ids = [];
		if ( $query->have_posts() ) {
			$post_ids = $query->posts;
			wp_reset_postdata();
		}
		// get terms values based on array of IDs.
		return wp_get_object_terms( $post_ids, Vertical::TAXONOMY );
	}
}
