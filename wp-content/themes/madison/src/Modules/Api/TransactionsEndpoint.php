<?php namespace Madison\Modules\Api;

use Lean\AbstractEndpoint;
use Madison\Modules\Transactions\Transactions as Transactions;

/**
 * Class used to generate a new endpoint transactions to generate a list with the different available
 * articles.
 */
class TransactionsEndpoint extends AbstractEndpoint {
	/**
	 * The variable used to identify the path of the endpooint.
	 *
	 * @var String
	 */
	protected $endpoint = '/transactions';

	/**
	 * When the endpoint is create create a new reference to the transactions class.
	 */
	public function __construct() {
		$this->transactions = new Transactions();
	}

	/**
	 * Function called by the endpoint if there are no more pages it returns a 404 reponse status.
	 * If html is specified as format param it returns the full HTML to be used on the Articles
	 * section.
	 *
	 * @param \WP_REST_Request $request Contains data from the request.
	 */
	public function endpoint_callback( \WP_REST_Request $request ) {
		$page = $request->get_param( 'page' );
		$year = $request->get_param( 'year' );
		$sector = $request->get_param( 'sector' );
		$list = $this->transactions->query( $page, $year, $sector );
		$total_pages = $this->transactions->total;

		// If we don't have more pages max_pages is zero but only if we are on a greather page than 1.
		if ( 0 === $total_pages && 1 < $page ) {
			return new \WP_Error( 'page_limit', 'Page not found.', [
				'status' => 404,
			]);
		}

		return $list;
	}

	/**
	 * Arguments that the endpoint can recieve, it accepts two params:
	 *
	 * - page: The number of page to request in the transactions
	 * - format: HTML if not specified it will return the collection of IDs.
	 *
	 * @return Array
	 */
	public function endpoint_args() {
		return [
			'page' => [
				'required' => false,
				'validate_callback' => function( $param ) {
					return is_numeric( $param );
				},
				'sanitize_callback' => 'absint',
				'default' => 1,
			],
			'year' => [
				'required' => false,
				'validate_callback' => function( $param ) {
					return is_numeric( $param );
				},
				'sanitize_callback' => 'absint',
			],
			'sector' => [
				'required' => false,
				'sanitize_callback' => 'sanitize_text_field',
				'default' => '',
			],
		];
	}
}
