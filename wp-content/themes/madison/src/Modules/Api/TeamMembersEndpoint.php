<?php namespace Madison\Modules\Api;

use Lean\AbstractEndpoint;
use Madison\Modules\TeamMembers\TeamMembers as TeamMember;

/**
 * Class used to generate a new endpoint for Team Members.
 */
class TeamMembersEndpoint extends AbstractEndpoint {
	/**
	 * The variable used to identify the path of the endpooint.
	 *
	 * @var String
	 */
	protected $endpoint = '/team-members';

	/**
	 * Function called by the endpoint if there are no more pages it returns a 404 reponse status.
	 * It returns the full HTML to be used on the Team Members module.
	 *
	 * @param \WP_REST_Request $request Contains data from the request.
	 */
	public function endpoint_callback( \WP_REST_Request $request ) {
		$page = $request->get_param( 'page' );
		$department = $request->get_param( 'department' );

		$args = [
			'query' => [
				'paged' => $page,
				'posts_per_page' => 9,
			],
			'return_html' => true,
		];

		if ( $department ) {
			$args['query']['tax_query'] = [
				[
					'taxonomy' => TeamMember::TAXONOMY,
					'terms' => $department,
				],
			];
		}

		$list = TeamMember::get_members_by_term( $args );

		// If we don't have more pages max_pages is zero but only if we are on a greather page than 1.
		if ( 0 === $list['max_pages'] && 1 < $page ) {
			return new \WP_Error( 'page_limit', 'Page not found.', [
				'max_pages' => 404,
			]);
		}

		return $list;
	}

	/**
	 * Arguments that the endpoint can receive, it accepts two params:
	 *
	 * - page: The number of page to request in the transactions
	 * - format: HTML if not specified it will return the collection of IDs.
	 *
	 * @return Array
	 */
	public function endpoint_args() {
		return [
			'page' => [
				'required' => false,
				'validate_callback' => function( $param ) {
					return is_numeric( $param );
				},
				'sanitize_callback' => 'absint',
				'default' => 1,
			],
			'department' => [
				'required' => false,
				'sanitize_callback' => 'sanitize_text_field',
				'default' => '',
			],
		];
	}
}
