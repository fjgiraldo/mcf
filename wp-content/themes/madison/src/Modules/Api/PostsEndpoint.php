<?php namespace Madison\Modules\Api;

use Lean\AbstractEndpoint;
use Madison\Modules\Posts\Posts;

/**
 * Class used to generate a new endpoint transactions to generate a list with the different available
 * articles.
 */
class PostsEndpoint extends AbstractEndpoint {
	/**
	 * The variable used to identify the path of the endpooint.
	 *
	 * @var String
	 */
	protected $endpoint = '/posts';

	/**
	 * When the endpoint is create create a new reference to the transactions class.
	 */
	public function __construct() {
		$this->posts = new Posts();
	}

	/**
	 * Function called by the endpoint if there are no more pages it returns a 404 reponse status.
	 * If html is specified as format param it returns the full HTML to be used on the Articles
	 * section.
	 *
	 * @param \WP_REST_Request $request Contains data from the request.
	 */
	public function endpoint_callback( \WP_REST_Request $request ) {
		$page = $request->get_param( 'page' );
		$news_type = $request->get_param( 'news_type' );
		$year = $request->get_param( 'year' );
		$media_type = $request->get_param( 'media_type' );
		$search_text = $request->get_param( 'search_text' );

		$args['paged'] = $page;

		if ( $news_type ) {
			$args['meta_query'] = [
				[
					'key' => 'news_type',
					'value' => $news_type,
				],
			];

			if ( 'insight' === $news_type ) {
				$args['posts_per_page'] = 3;
			}
		}

		if ( $year ) {
			$args['date_query'] = [
				[
					'year' => $year,
				],
			];
		}

		if ( $media_type ) {
			$args['tax_query'] = [
				[
					'taxonomy' => 'category',
					'field' => 'slug',
					'terms' => $media_type,
				],
			];
		}

		if ( $search_text ) {
			$args['s'] = $search_text;
		}

		$list = $this->posts->query( $args );
		$total_pages = $this->posts->total;

		// If we don't have more pages max_pages is zero but only if we are on a greather page than 1.
		if ( 0 === $total_pages && 1 < $page ) {
			return new \WP_Error( 'page_limit', 'Page not found.', [
				'status' => 404,
			]);
		}

		return $list;
	}

	/**
	 * Arguments that the endpoint can recieve, it accepts two params:
	 *
	 * - page: The number of page to request in the transactions
	 * - format: HTML if not specified it will return the collection of IDs.
	 *
	 * @return Array
	 */
	public function endpoint_args() {
		return [
			'page' => [
				'required' => false,
				'validate_callback' => function( $param ) {
					return is_numeric( $param );
				},
				'sanitize_callback' => 'absint',
				'default' => 1,
			],
			'news_type' => [
				'required' => true,
				'sanitize_callback' => 'sanitize_text_field',
				'default' => 'media',
			],
			'year' => [
				'required' => false,
				'validate_callback' => function( $param ) {
					return is_numeric( $param );
				},
				'sanitize_callback' => 'absint',
			],
			'media_type' => [
				'required' => false,
				'sanitize_callback' => 'sanitize_text_field',
				'default' => '',
			],
			'search_text' => [
				'required' => false,
				'sanitize_callback' => 'sanitize_text_field',
				'default' => '',
			],
		];
	}
}
