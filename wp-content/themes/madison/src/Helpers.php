<?php namespace Madison;

/**
 * Class that register the menu locations to be used with the site.
 */
class Helpers {
	/**
	 * Function that counts the number of decimals in a number.
	 *
	 * @param number $number The number.
	 * @return number
	 */
	public static function count_decimals( $number ) {
		return strlen( strrchr( $number, '.' ) ) - 1;
	}

	/**
	 * Returns excerpt if available, or else trims the content by the given number of words.
	 *
	 * @param int $post_id The post id.
	 * @param int $num_words Given number of words to trim.
	 * @return string excerpt/modified content.
	 */
	public static function custom_excerpt( $post_id, $num_words ) {
		$excerpt = get_post_field( 'post_excerpt', $post_id );
		$content = strip_shortcodes( get_post_field( 'post_content', $post_id ) );
		return ( '' !== $excerpt ) ? $excerpt : wp_trim_words( $content, $num_words, '...' );
	}

	/**
	 * Function to create a value that can be used as an ID HTML attribute.
	 *
	 * @param string $title The title or string to be converted to ID.
	 * @param string $default If title is empty this string is used with the page id to prevent coallitions.
	 * @return string
	 */
	public static function create_id_from( $title, $default ) {
		$anchor_id = empty( $title ) ? $default . '-' . get_the_ID() : $title;
		return sanitize_title( strtolower( trim( $anchor_id ) ) );
	}

	/**
	 * Returns css code to give an element with $class, a responsive backgroud image with retina and non retina support.
	 *
	 * @param int    $attachment_id The image ID.
	 * @param string $target The class or ID of the element.
	 */
	public static function bg_image( $attachment_id, $target ) {
		$image_metadata = (array) wp_get_attachment_metadata( $attachment_id );
		$width_half = $image_metadata['width'] / 2;
		$height_half = $image_metadata['height'] / 2;
		$image_2x_desktop = (array) wp_get_attachment_image_src( $attachment_id, 'full' );
		$image_2x_mobile = (array) wp_get_attachment_image_src( $attachment_id, 'container' );
		$image_1x_desktop = (array) wp_get_attachment_image_src( $attachment_id, [ $width_half, $height_half ] );
		?>
		<style>
		<?php echo esc_html( $target ); ?> {
			background-image: url(<?php echo esc_url( $image_1x_desktop[0] ); ?>);
		}
		@media screen and (max-width:425px) {
			<?php echo esc_html( $target ); ?> {
				background-image: url(<?php echo esc_url( $image_2x_mobile[0] ); ?>);
			}
		}		
		@media screen and (-webkit-min-device-pixel-ratio : 1.5),
		 screen and (-o-min-device-pixel-ratio: 3/2),
		 screen and (min--moz-device-pixel-ratio: 1.5),
		 screen and (min-device-pixel-ratio: 1.5) {
			<?php echo esc_html( $target ); ?> {
				background-image: url(<?php echo esc_url( $image_2x_desktop[0] ); ?>);
			}
		}			
		</style>
		<?php
	}
}
