<?php namespace Madison;

/**
 * General admin interface
 */
class Admin {
	/**
	 * Init.
	 */
	public static function init() {
		if ( ! is_admin() ) {
			return;
		}

		add_action( 'admin_menu', [ __CLASS__, 'hide_menu_items' ] );
		add_action( 'admin_menu', [ __CLASS__, 'show_gform_menu_item' ] );
		add_action( 'admin_menu', [ __CLASS__, 'change_post_menu_label' ] );
		add_filter( 'wpseo_metabox_prio', [ __CLASS__, 'yoast_priority' ] );
		add_filter( 'mce_buttons_2', [ __CLASS__, 'enable_format_option' ] );
		add_filter( 'tiny_mce_before_init', [ __CLASS__, 'add_format_option' ] );
		add_action( 'init', [ __CLASS__, 'editor_custom_styles' ] );
		add_action( 'init', [ __CLASS__, 'change_post_page_label' ] );
		add_action( 'init', [ __CLASS__, 'disable_post_tags' ] );
		add_action( 'admin_enqueue_scripts', [ __CLASS__, 'add_custom_admin_assets' ] );
	}

	/**
	 * Function that hides menu items from the users that does not have
	 * the administrator role.
	 */
	public static function hide_menu_items() {
		if ( self::is_administrator( \wp_get_current_user() ) ) {
			return;
		}
		remove_menu_page( 'tools.php' );
		remove_menu_page( 'themes.php' );
		remove_menu_page( 'edit-comments.php' );
	}

	/**
	 * Function that test if the user has the administrator role
	 *
	 * @param \WP_User $user The user to test.
	 * @return bool true if the user has the administrator role false otherwise.
	 */
	public static function is_administrator( \WP_User $user ) {
		return in_array( 'administrator', $user->roles, true );
	}

	/**
	 * Change the priority used for the YOAST SEO Meta Box so ACF Boxes and other
	 * components or meta fields.
	 *
	 * @return string The new priority
	 */
	public static function yoast_priority() {
		return 'low';
	}

	/**
	 * Add js and css files to admin pages.
	 *
	 * @param string $hook actual page url part.
	 */
	public static  function add_custom_admin_assets( $hook ) {
		wp_enqueue_script( 'admin_posts_js', get_template_directory_uri() . '/admin/admin.js' );
		wp_enqueue_style( 'admin_posts_css', get_template_directory_uri() . '/admin/admin.css' );
	}

	/**
	 * Add a custom stylesheet to the Wysiwyg editor.
	 */
	public static function editor_custom_styles() {
		add_editor_style( 'admin/wysiwyg.css' );
	}

	/**
	 * Enables gform admin menu item to the Editor role.
	 */
	public static function show_gform_menu_item() {
		$role = get_role( 'editor' );
		if ( $role instanceof WP_Role ) {
			$role->add_cap( 'gform_full_access' );
		}
	}

	/**
	 * Add a format option to the Wysiwyg editor.
	 *
	 * @param array $init_array the dropdown data.
	 * @return array the changed dropdown data.
	 */
	public static function add_format_option( $init_array ) {
		$style_formats = [
			[
				'title' => 'Small Text',
				'block' => 'span',
				'classes' => 'content-small',
				'wrapper' => true,
			],
		];
		// Insert the array, JSON ENCODED, into 'style_formats'.
		$init_array['style_formats'] = wp_json_encode( $style_formats );
		return $init_array;
	}

	/**
	 * Enable Format dropdown on the Wysiwyg editor
	 *
	 * @param array $buttons The button.
	 * @return string The changed button.
	 */
	public static function enable_format_option( $buttons ) {
		array_unshift( $buttons, 'styleselect' );
		return $buttons;
	}

	/**
	 * Change Posts label in admin sidemenu
	 */
	public static function change_post_menu_label() {
		global $menu;
		$menu[5][0] = 'Media & Insight Posts';
		echo '';
	}

	/**
	 * Change Posts label in admin Posts edit page
	 */
	public static function change_post_page_label() {
		global $wp_post_types;
		$labels = &$wp_post_types['post']->labels;
		$labels->name = 'Media & Insight Posts';
	}

	/**
	 * Disable tags for posts.
	 */
	public static function disable_post_tags() {
		unregister_taxonomy_for_object_type( 'post_tag', 'post' );
	}
}
